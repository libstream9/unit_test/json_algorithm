#include <stream9/json/algorithm/create_path.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(create_path_)

    BOOST_AUTO_TEST_CASE(empty_pointer_return_value_itself_)
    {
        json::value root;
        auto& leaf = json::create_path(root, ""_jp);

        BOOST_TEST(&leaf == &root);
    }

    BOOST_AUTO_TEST_CASE(object_with_empty_key_1_)
    {
        json::value root;
        auto& leaf = json::create_path(root, "/"_jp);

        BOOST_TEST(root == R"({ "": null })"_js);
        BOOST_TEST(leaf == R"(null)"_js);

        leaf = 100;
        BOOST_TEST(root == R"({ "": 100 })"_js);
    }

    BOOST_AUTO_TEST_CASE(object_with_empty_key_2_)
    {
        json::value root;
        auto& leaf = json::create_path(root, "//"_jp);

        BOOST_TEST(root == R"({ "": { "": null } })"_js);
        BOOST_TEST(leaf == "null"_js);

        leaf = 100;
        BOOST_TEST(root == R"({ "": { "": 100 } })"_js);
    }

    BOOST_AUTO_TEST_CASE(object_1_)
    {
        json::value root;
        auto& leaf = json::create_path(root, "/foo"_jp);

        BOOST_TEST(root == R"({ "foo": null })"_js);
        BOOST_TEST(leaf == "null"_js);

        leaf = 100;
        BOOST_TEST(root == R"({ "foo": 100 })"_js);
    }

    BOOST_AUTO_TEST_CASE(object_2_)
    {
        json::value root;
        auto& leaf = json::create_path(root, "/foo/bar"_jp);

        BOOST_TEST(root == R"({ "foo": { "bar": null } })"_js);
        BOOST_TEST(leaf == R"(null)"_js);

        leaf = 100;
        BOOST_TEST(root == R"({ "foo": { "bar": 100 } })"_js);
    }

    BOOST_AUTO_TEST_CASE(object_3_)
    {
        auto root = R"({ "foo": 1 })"_js;
        auto& leaf = json::create_path(root, "/foo/bar"_jp);

        BOOST_TEST(root == R"({ "foo": { "bar": null } })"_js);
        BOOST_TEST(leaf == R"(null)"_js);

        leaf = 100;
        BOOST_TEST(root == R"({ "foo": { "bar": 100 } })"_js);
    }

    BOOST_AUTO_TEST_CASE(object_4_)
    {
        auto root = R"({ "foo": { "baz": 1 } })"_js;
        auto& leaf = json::create_path(root, "/foo/bar"_jp);

        BOOST_TEST(root == R"({ "foo": { "bar": null, "baz": 1 } })"_js);
        BOOST_TEST(leaf == R"(null)"_js);

        leaf = 100;
        BOOST_TEST(root == R"({ "foo": { "bar": 100, "baz": 1 } })"_js);
    }

    BOOST_AUTO_TEST_CASE(object_key_already_exist_)
    {
        auto root = R"({ "foo": 1 })"_js;
        auto& leaf = json::create_path(root, "/foo"_jp);

        BOOST_TEST(root == R"({ "foo": 1 })"_js);
        BOOST_TEST(leaf == 1);

        leaf = 100;
        BOOST_TEST(root == R"({ "foo": 100 })"_js);
    }

    BOOST_AUTO_TEST_CASE(array_1)
    {
        json::value root;
        auto& leaf = json::create_path(root, "/0"_jp);

        BOOST_TEST(root == R"([ null ])"_js);
        BOOST_TEST(leaf == "null"_js);

        leaf = 100;
        BOOST_TEST(root == R"([ 100 ])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_2)
    {
        json::value root;
        auto& leaf = json::create_path(root, "/0/-"_jp);

        BOOST_TEST(root == R"([ [ null ] ])"_js);
        BOOST_TEST(leaf == R"(null)"_js);

        leaf = 100;
        BOOST_TEST(root == R"([ [ 100 ] ])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_3)
    {
        json::value root;
        auto& leaf = json::create_path(root, "/1"_jp);

        BOOST_TEST(root == R"([ null, null ])"_js);
        BOOST_TEST(leaf == R"(null)"_js);

        leaf = 100;
        BOOST_TEST(root == R"([ null, 100 ])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_key_already_exist_)
    {
        auto root = R"([ null ])"_js;
        auto& leaf = json::create_path(root, "/0"_jp);

        BOOST_TEST(root == R"([ null ])"_js);
        BOOST_TEST(leaf == nullptr);

        leaf = 100;
        BOOST_TEST(root == R"([ 100 ])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_index_on_object_)
    {
        auto root = R"({ "foo": 1 })"_js;
        auto& leaf = json::create_path(root, "/1"_jp);

        BOOST_TEST(root == R"({ "foo": 1, "1": null })"_js);
        BOOST_TEST(leaf == R"(null)"_js);

        leaf = 100;
        BOOST_TEST(root == R"({ "foo": 1, "1": 100 })"_js);
    }

    BOOST_AUTO_TEST_CASE(invalid_array_index_1_)
    {
        auto root = R"([])"_js;
        auto& leaf = json::create_path(root, "/foo"_jp);

        BOOST_TEST(root == R"({ "foo": null })"_js);
        BOOST_TEST(leaf == nullptr);

        leaf = 100;
        BOOST_TEST(root == R"({ "foo": 100 })"_js);
    }

    BOOST_AUTO_TEST_CASE(invalid_array_index_2_)
    {
        auto root = R"([])"_js;
        auto& leaf = json::create_path(root, "/-1"_jp);

        BOOST_TEST(root == R"({ "-1": null })"_js);
        BOOST_TEST(leaf == nullptr);

        leaf = 100;
        BOOST_TEST(root == R"({ "-1": 100 })"_js);
    }

    BOOST_AUTO_TEST_CASE(not_overwrite_null_)
    {
        auto root = R"({ "a": null })"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::create_path(root, "/a/b"_jp, {
                .overwrite_null_on_the_path = false,
            }),
            json::pointer::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::pointer::error",
                    "code": "object key on null",
                    "pointer": "/a/b",
                    "pos": 3
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(null_as_new_value_is_overwritten_anyway_)
    {
        auto root = R"({ })"_js;

        auto& leaf = json::create_path(root, "/a/b"_jp, {
            .overwrite_null_on_the_path = false,
        });

        BOOST_TEST(root == R"({ "a": { "b": null } })"_js);
        BOOST_TEST(leaf == nullptr);

        leaf = 100;
        BOOST_TEST(root == R"({ "a": { "b": 100 } })"_js);
    }

    BOOST_AUTO_TEST_CASE(not_overwrite_scalar_)
    {
        auto root = R"([100, 200])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::create_path(root, "/0/0"_jp, {
                .overwrite_scalar_on_the_path = false,
            }),
            json::pointer::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::pointer::error",
                    "code": "array index on scalar",
                    "pointer": "/0/0",
                    "pos": 3
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(not_overwrite_array_)
    {
        auto root = R"([1, 2])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::create_path(root, "/foo"_jp, {
                .overwrite_array_when_token_is_not_array_index = false,
            } ),
            json::pointer::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/foo",
                    "pos": 1
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(not_full_array_)
    {
        auto root = R"([])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::create_path(root, "/2"_jp, {
                .create_empty_value_on_array = false,
            } ),
            json::pointer::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::pointer::error",
                    "code": "array index out of bound",
                    "pointer": "/2",
                    "pos": 1
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(see_all_token_as_object_key_)
    {
        auto root = R"({})"_js;

        auto& leaf = json::create_path(root, "/0/-"_jp, {
            .regard_all_token_of_absent_path_as_object_key = true,
        });

        BOOST_TEST(root == R"({ "0": { "-": null } })"_js);
        BOOST_TEST(leaf == nullptr);

        leaf = 100;
        BOOST_TEST(root == R"({ "0": { "-": 100 } })"_js);
    }

BOOST_AUTO_TEST_SUITE_END() // create_path_

} // namespace testing
