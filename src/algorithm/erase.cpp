#include <stream9/json/algorithm/erase.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace json::literals;

BOOST_AUTO_TEST_SUITE(erase_)

    BOOST_AUTO_TEST_CASE(empty_pointer_)
    {
        json::value p1;

        BOOST_CHECK_EXCEPTION(
            json::erase(p1, ""_jp),
            json::pointer::error,
            [](auto&& e) {
                using E = json::pointer::errc;
                BOOST_TEST(e.code() == E::erase_with_empty_pointer);
                BOOST_TEST(e.pointer() == "");
                BOOST_TEST(e.pos() == 0);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(erase_from_object_1_)
    {
        auto p1 = R"({ "foo": 100 })"_js;

        auto& v1 = json::erase(p1, "/foo"_jp);

        BOOST_TEST(p1 == "{}"_js);
        BOOST_TEST(v1 == p1);
    }

    BOOST_AUTO_TEST_CASE(erase_from_object_2_)
    {
        auto p1 = R"({
          "foo": {
            "bar": 200
          }
        })"_js;

        auto& v1 = json::erase(p1, "/foo/bar"_jp);

        BOOST_TEST(p1 == R"({ "foo": {} })"_js);

        v1 = 100;
        BOOST_TEST(p1 == R"({ "foo": 100 })"_js);
    }

    BOOST_AUTO_TEST_CASE(erase_from_array_1_)
    {
        auto p1 = R"([ 100, 200 ])"_js;

        auto& v1 = json::erase(p1, "/1"_jp);

        BOOST_TEST(p1 == R"([ 100 ])"_js);
        BOOST_TEST(v1 == p1);
    }

    BOOST_AUTO_TEST_CASE(erase_from_array_2_)
    {
        auto p1 = R"([ 100, 200, { "foo": 100 } ])"_js;

        auto& v1 = json::erase(p1, "/2/foo"_jp);

        BOOST_TEST(p1 == R"([ 100, 200, {} ])"_js);

        v1 = 500;
        BOOST_TEST(p1 == R"([ 100, 200, 500 ])"_js);
    }

    BOOST_AUTO_TEST_CASE(erase_non_existant_key_)
    {
        auto p1 = R"({ "foo": 100 })"_js;

        BOOST_CHECK_EXCEPTION(
            json::erase(p1, "/bar"_jp),
            json::pointer::error,
            [](auto&& e) {
                using E = json::pointer::errc;
                BOOST_TEST(e.code() == E::object_key_does_not_exist);
                BOOST_TEST(e.pointer() == "/bar");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(erase_from_array_with_invalid_index_)
    {
        auto p1 = R"([ 100, 200 ])"_js;

        BOOST_CHECK_EXCEPTION(
            json::erase(p1, "/foo"_jp),
            json::pointer::error,
            [](auto&& e) {
                using E = json::pointer::errc;
                BOOST_TEST(e.code() == E::invalid_array_index);
                BOOST_TEST(e.pointer() == "/foo");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(erase_from_array_with_out_of_bound_index_)
    {
        auto p1 = R"([ 100, 200 ])"_js;

        BOOST_CHECK_EXCEPTION(
            json::erase(p1, "/2"_jp),
            json::pointer::error,
            [](auto&& e) {
                using E = json::pointer::errc;
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                BOOST_TEST(e.pointer() == "/2");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(erase_from_array_with_index_with_leading_zero_)
    {
        auto p1 = R"([ 100, 200 ])"_js;

        BOOST_CHECK_EXCEPTION(
            json::erase(p1, "/01"_jp),
            json::pointer::error,
            [](auto&& e) {
                using E = json::pointer::errc;
                BOOST_TEST(e.code() == E::array_index_has_leading_zero);
                BOOST_TEST(e.pointer() == "/01");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(erase_from_scalar_with_object_key_)
    {
        auto p1 = R"({ "foo": 100 })"_js;

        BOOST_CHECK_EXCEPTION(
            json::erase(p1, "/foo/bar"_jp),
            json::pointer::error,
            [](auto&& e) {
                using E = json::pointer::errc;
                BOOST_TEST(e.code() == E::object_key_on_scalar);
                BOOST_TEST(e.pointer() == "/foo/bar");
                BOOST_TEST(e.pos() == 5);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(erase_from_scalar_with_array_index_)
    {
        auto p1 = R"({ "foo": 100 })"_js;

        BOOST_CHECK_EXCEPTION(
            json::erase(p1, "/foo/0"_jp),
            json::pointer::error,
            [](auto&& e) {
                using E = json::pointer::errc;
                BOOST_TEST(e.code() == E::array_index_on_scalar);
                BOOST_TEST(e.pointer() == "/foo/0");
                BOOST_TEST(e.pos() == 5);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(on_lvalue_object_)
    {
        auto v1 = R"({ "foo": 100 })"_js;

        auto& v2 = json::erase(v1.get_object(), "/foo"_jp);

        BOOST_TEST(v1 == R"({})"_js);
        BOOST_TEST(&v2 == &v1);
    }

    BOOST_AUTO_TEST_CASE(on_lvalue_array_)
    {
        auto v1 = "[ 100 ]"_js;

        auto& v2 = json::erase(v1.get_array(), "/0"_jp);

        BOOST_TEST(v1 == "[]"_js);
        BOOST_TEST(&v2 == &v1);
    }

BOOST_AUTO_TEST_SUITE_END() // erase_

} // namespace testing
