#include <stream9/json/algorithm/find.hpp>

#include "../namespace.hpp"

//#include <stream9/json/unintended_copy_workaround.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace json::literals;

using E = json::pointer::errc;

BOOST_AUTO_TEST_SUITE(find_)

    BOOST_AUTO_TEST_CASE(found_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        {
            auto* o_v = json::find(v1, "/foo"_jp);
            static_assert(std::same_as<decltype(o_v), json::value*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(o_v->is_object());
        }

        {
            auto* o_v = json::find(v1, "/foo/bar"_jp);
            static_assert(std::same_as<decltype(o_v), json::value*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(*o_v == 200);
        }

        {
            auto* o_v = json::find(v1, "/foo/baz"_jp);
            static_assert(std::same_as<decltype(o_v), json::value*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(o_v->is_array());
        }

        {
            auto* o_v = json::find(v1, "/foo/baz/0"_jp);
            static_assert(std::same_as<decltype(o_v), json::value*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(*o_v == 100);
        }

        {
            auto* o_v = json::find(v1, "/foo/baz/2"_jp);
            static_assert(std::same_as<decltype(o_v), json::value*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(*o_v == 300);
        }
    }

    BOOST_AUTO_TEST_CASE(not_found_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto* o_v1 = json::find(v1, "/foo/foo"_jp);
        BOOST_TEST(!o_v1);

        auto* o_v2 = json::find(v1, "/bar"_jp);
        BOOST_TEST(!o_v2);

        auto* o_v3 = json::find(v1, "/foo/baz/3"_jp);
        BOOST_TEST(!o_v3);

        auto* o_v4 = json::find(v1, "/foo/baz/-"_jp);
        BOOST_TEST(!o_v4);
    }

    BOOST_AUTO_TEST_CASE(on_object_)
    {
        auto v1 = R"({ "foo": 100, "bar": 200 })"_js;

        auto o_v = json::find(v1.get_object(), "/bar"_jp);
        BOOST_TEST(o_v);

        *o_v = 500;
        BOOST_TEST(v1 == R"({ "foo": 100, "bar": 500 })"_js);
    }

    BOOST_AUTO_TEST_CASE(on_object_2_)
    {
        json::object o1 { { "foo", 100 }, { "bar", 200 } };

        auto o_v = json::find(o1, "/bar"_jp);
        BOOST_TEST(o_v);

        *o_v = 500;
        BOOST_TEST(o1 == R"({ "foo": 100, "bar": 500 })"_js);
    }

    BOOST_AUTO_TEST_CASE(on_array_)
    {
        auto v1 = R"([ 100, 200 ])"_js;

        auto o_v = json::find(v1.get_array(), "/1"_jp);
        BOOST_TEST(o_v);

        *o_v = 500;
        BOOST_TEST(v1 == R"([ 100, 500 ])"_js);
    }

    BOOST_AUTO_TEST_CASE(on_string_)
    {
        auto v1 = R"("foo")"_js;
        BOOST_TEST_REQUIRE(v1.is_string());

        auto o_v2 = json::find(v1.get_string(), ""_jp);
        BOOST_TEST_REQUIRE(o_v2);
        *o_v2 = "bar";
        BOOST_TEST(v1 == R"("bar")"_js);

        auto o_v3 = json::find(v1.get_string(), "/foo"_jp);
        BOOST_TEST(!o_v3);
    }

BOOST_AUTO_TEST_SUITE_END() // find_

BOOST_AUTO_TEST_SUITE(find_const_)

    BOOST_AUTO_TEST_CASE(found_)
    {
        auto const v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        {
            auto* o_v = json::find(v1, "/foo"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(o_v->is_object());
        }

        {
            auto* o_v = json::find(v1, "/foo/bar"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(*o_v == 200);
        }

        {
            auto* o_v = json::find(v1, "/foo/baz"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(o_v->is_array());
        }

        {
            auto* o_v = json::find(v1, "/foo/baz/0"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(*o_v == 100);
        }

        {
            auto* o_v = json::find(v1, "/foo/baz/2"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST_REQUIRE(o_v);
            BOOST_TEST(*o_v == 300);
        }
    }

    BOOST_AUTO_TEST_CASE(not_found_)
    {
        auto const v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        {
            auto* o_v = json::find(v1, "/foo/foo"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST(!o_v);
        }

        {
            auto* o_v = json::find(v1, "/bar"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST(!o_v);
        }

        {
            auto* o_v = json::find(v1, "/foo/baz/3"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST(!o_v);
        }

        {
            auto* o_v = json::find(v1, "/foo/baz/-"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST(!o_v);
        }

        {
            auto* o_v = json::find(v1, "/foo/xyzzy/-"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST(!o_v);
        }

        {
            auto* o_v = json::find(v1, "/foo/xyzzy/foo"_jp);
            static_assert(std::same_as<decltype(o_v), json::value const*>);
            BOOST_TEST(!o_v);
        }
    }

    BOOST_AUTO_TEST_CASE(on_object_)
    {
        auto const v1 = R"({ "foo": 100, "bar": 200 })"_js;

        auto o_v2 = json::find(v1, "/bar"_jp);
        auto o_v3 = json::find(v1.get_object(), "/bar"_jp);

        BOOST_TEST_REQUIRE(o_v2);
        BOOST_TEST_REQUIRE(o_v3);
        BOOST_TEST(&*o_v2 == &*o_v3);
    }

    BOOST_AUTO_TEST_CASE(on_array_)
    {
        auto const v1 = R"([ 100, 200 ])"_js;

        auto o_v2 = json::find(v1, "/1"_jp);
        BOOST_TEST_REQUIRE(o_v2);

        auto o_v3 = json::find(v1.get_array(), "/1"_jp);
        BOOST_TEST_REQUIRE(o_v3);

        BOOST_TEST(&*o_v2 == &*o_v3);
    }

    BOOST_AUTO_TEST_CASE(on_string_)
    {
        auto const v1 = R"("foo")"_js;
        BOOST_TEST_REQUIRE(v1.is_string());

        auto o_v2 = json::find(v1.get_string(), ""_jp);
        BOOST_TEST_REQUIRE(o_v2);
        BOOST_TEST(&*o_v2 == &v1);

        auto o_v3 = json::find(v1.get_string(), "/foo"_jp);
        BOOST_TEST(!o_v3);
    }

BOOST_AUTO_TEST_SUITE_END() // find_const_

BOOST_AUTO_TEST_SUITE(find_throw_)

    BOOST_AUTO_TEST_CASE(find_1_)
    {
        auto v1 = R"({
          "foo": 100
        })"_js;

        auto expected = v1;

        auto& v2 = json::find_or_throw(v1, "/foo"_jp);

        BOOST_TEST(v1 == expected);
        BOOST_TEST(v2 == 100);
    }

    BOOST_AUTO_TEST_CASE(find_2_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200
          }
        })"_js;

        auto expected = v1;

        auto& v2 = json::find_or_throw(v1, "/foo/bar"_jp);

        BOOST_TEST(v1 == expected);
        BOOST_TEST(v2 == 200);
    }

    BOOST_AUTO_TEST_CASE(find_3_)
    {
        auto v1 = R"({
          "foo": [ 100, 200 ]
        })"_js;

        auto expected = v1;

        auto& v2 = json::find_or_throw(v1, "/foo/1"_jp);

        BOOST_TEST(v1 == expected);
        BOOST_TEST(v2 == 200);
    }

    BOOST_AUTO_TEST_CASE(throw_1_)
    {
        auto v1 = R"({
          "foo": 100
        })"_js;

        auto expected = v1;

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1, "/bar"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::object_key_does_not_exist);
                BOOST_TEST(e.pointer() == "/bar");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(throw_2_)
    {
        auto v1 = R"([ 100 ])"_js;

        auto expected = v1;

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1, "/bar"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::invalid_array_index);
                BOOST_TEST(e.pointer() == "/bar");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(throw_3_)
    {
        auto v1 = R"([ 100 ])"_js;

        auto expected = v1;

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1, "/2"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                BOOST_TEST(e.pointer() == "/2");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(throw_4_)
    {
        auto v1 = R"([ 100 ])"_js;

        auto expected = v1;

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1, "/-"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                BOOST_TEST(e.pointer() == "/-");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(throw_5_)
    {
        auto v1 = R"({
          "foo": 100
        })"_js;

        auto expected = v1;

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1, "/foo/bar"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::object_key_on_scalar);
                BOOST_TEST(e.pointer() == "/foo/bar");
                BOOST_TEST(e.pos() == 5);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(throw_6_)
    {
        auto v1 = R"({
          "foo": 100
        })"_js;

        auto expected = v1;

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1, "/foo/0"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_on_scalar);
                BOOST_TEST(e.pointer() == "/foo/0");
                BOOST_TEST(e.pos() == 5);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(on_object_)
    {
        auto v1 = R"({ "foo": 100 })"_js;
        BOOST_TEST_REQUIRE(v1.is_object());

        auto& v2 = json::find_or_throw(v1.get_object(), "/foo"_jp);
        static_assert(std::same_as<decltype(v1.get_object()), json::object&>);
        static_assert(std::same_as<decltype(v2), json::value&>);
        BOOST_TEST(v2 == 100);

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1.get_object(), "/bar"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::object_key_does_not_exist);
                BOOST_TEST(e.pointer() == "/bar");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(on_const_object_)
    {
        auto const v1 = R"({ "foo": 100 })"_js;
        BOOST_TEST_REQUIRE(v1.is_object());

        auto& v2 = json::find_or_throw(v1.get_object(), "/foo"_jp);
        static_assert(std::same_as<decltype(v2), json::value const&>);
        BOOST_TEST(v2 == 100);

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1.get_object(), "/bar"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::object_key_does_not_exist);
                BOOST_TEST(e.pointer() == "/bar");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(on_array_)
    {
        auto v1 = "[ 100, 200 ]"_js;
        BOOST_TEST_REQUIRE(v1.is_array());

        auto& v2 = json::find_or_throw(v1.get_array(), "/1"_jp);
        static_assert(std::same_as<decltype(v2), json::value&>);
        BOOST_TEST(v2 == 200);

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1.get_array(), "/2"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                BOOST_TEST(e.pointer() == "/2");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(on_const_array_)
    {
        auto const v1 = "[ 100, 200 ]"_js;
        BOOST_TEST_REQUIRE(v1.is_array());

        auto& v2 = json::find_or_throw(v1.get_array(), "/1"_jp);
        static_assert(std::same_as<decltype(v2), json::value const&>);
        BOOST_TEST(v2 == 200);

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1.get_array(), "/2"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                BOOST_TEST(e.pointer() == "/2");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(on_string_)
    {
        auto v1 = R"("foo")"_js;
        BOOST_TEST_REQUIRE(v1.is_string());

        auto& v2 = json::find_or_throw(v1.get_string(), ""_jp);
        static_assert(std::same_as<decltype(v2), json::value&>);
        BOOST_TEST(&v2 == &v1);

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1.get_string(), "/-"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_on_scalar);
                BOOST_TEST(e.pointer() == "/-");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(on_const_string_)
    {
        auto const v1 = R"("foo")"_js;
        BOOST_TEST_REQUIRE(v1.is_string());

        auto& v2 = json::find_or_throw(v1.get_string(), ""_jp);
        static_assert(std::same_as<decltype(v2), json::value const&>);
        BOOST_TEST(&v2 == &v1);

        BOOST_CHECK_EXCEPTION(
            json::find_or_throw(v1.get_string(), "/-"_jp),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_on_scalar);
                BOOST_TEST(e.pointer() == "/-");
                BOOST_TEST(e.pos() == 1);
                return true;
            }
        );
    }

BOOST_AUTO_TEST_SUITE_END() // find_throw_

} // namespace testing
