#include <stream9/json/algorithm/insert.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace json::literals;

using E = json::pointer::errc;

BOOST_AUTO_TEST_SUITE(insert_)

    BOOST_AUTO_TEST_CASE(insert_value_on_null_with_empty_pointer_)
    {
        json::value v1;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, ""_jp, 100),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::insert_with_empty_pointer);
                BOOST_TEST(e.pointer() == "");
                BOOST_TEST(e.pos() == 0);
                return true;
            } );

        BOOST_TEST(v1 == "null"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_scalar_with_empty_pointer_)
    {
        auto v1 = R"(100)"_js;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, ""_jp, 200),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::insert_with_empty_pointer);
                BOOST_TEST(e.pointer() == "");
                BOOST_TEST(e.pos() == 0);
                return true;
            } );

        BOOST_TEST(v1 == "100"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_object_with_empty_pointer_)
    {
        auto v1 = R"({ "foo": 100 })"_js;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, ""_jp, 200),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::insert_with_empty_pointer);
                BOOST_TEST(e.pointer() == "");
                BOOST_TEST(e.pos() == 0);
                return true;
            } );

        auto expected = R"({ "foo": 100 })"_js;
        BOOST_TEST(v1 == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_with_empty_pointer_)
    {
        auto v1 = R"([ 1, 2, 3 ])"_js;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, ""_jp, 200),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::insert_with_empty_pointer);
                BOOST_TEST(e.pointer() == "");
                BOOST_TEST(e.pos() == 0);
                return true;
            } );

        BOOST_TEST(v1 == "[1, 2, 3]"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_null_with_object_key_)
    {
        json::value v1;

        auto& v2 = json::insert(v1, "/foo"_jp, 100);

        BOOST_TEST(v1 == R"({ "foo": 100 })"_js);

        v2 = "xxx";
        BOOST_TEST(v1 == R"({ "foo": "xxx" })"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_null_with_array_idx_1_)
    {
        json::value v1;

        auto& v2 = json::insert(v1, "/-"_jp, 100);

        BOOST_TEST(v1 == "[ 100 ]"_js);

        v2 = "xxx";
        BOOST_TEST(v1 == R"([ "xxx" ])"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_null_with_array_idx_2_)
    {
        json::value v1;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, "/200"_jp, 100),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                BOOST_TEST(e.pointer() == "/200");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );

        BOOST_TEST(v1.is_null());
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_object_with_object_key_)
    {
        auto v1 = R"({})"_js;

        auto& v2 = json::insert(v1, "/foo"_jp, 100);

        BOOST_TEST(v1 == R"({ "foo": 100 })"_js);

        v2 = "xxx";
        BOOST_TEST(v1 == R"({ "foo": "xxx" })"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_object_with_existing_object_key_)
    {
        auto v1 = R"({ "foo": 200 })"_js;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, "/foo"_jp, 100),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::object_key_already_exist);
                BOOST_TEST(e.pointer() == "/foo");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );

        BOOST_TEST(v1 == R"({ "foo": 200 })"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_object_with_array_idx_)
    {
        auto v1 = R"({})"_js;

        auto& v2 = json::insert(v1, "/0"_jp, 100);

        BOOST_TEST(v1 == R"({ "0": 100 })"_js);

        v2 = "xxx";
        BOOST_TEST(v1 == R"({ "0": "xxx" })"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_object_with_empty_token_1_)
    {
        auto v1 = R"({})"_js;

        auto& v2 = json::insert(v1, "/"_jp, 100);

        BOOST_TEST(v1 == R"({ "": 100 })"_js);

        v2 = "xxx";
        BOOST_TEST(v1 == R"({ "": "xxx" })"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_with_array_idx_1)
    {
        auto v1 = R"([ 100 ])"_js;

        auto& v2 = json::insert(v1, "/0"_jp, 200);

        BOOST_TEST(v1 == R"([ 200, 100 ])"_js);

        v2 = 300;
        BOOST_TEST(v1 == R"([ 300, 100 ])"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_with_array_idx_2)
    {
        auto v1 = R"([ 100 ])"_js;
        auto v2 = 200;

        auto& v3 = json::insert(v1, "/-"_jp, v2);

        BOOST_TEST(v1 == R"([ 100, 200 ])"_js);

        v3 = 300;
        BOOST_TEST(v1 == R"([ 100, 300 ])"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_with_array_idx_3)
    {
        auto v1 = R"([ 100 ])"_js;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, "/2"_jp, 200),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                BOOST_TEST(e.pointer() == "/2");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );

        auto expected = R"([ 100 ])"_js;
        BOOST_TEST(v1 == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_with_array_idx_4)
    {
        auto v1 = R"([ 100 ])"_js;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, "/00"_jp, 200),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_has_leading_zero);
                BOOST_TEST(e.pointer() == "/00");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );

        auto expected = R"([ 100 ])"_js;
        BOOST_TEST(v1 == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_with_object_key_)
    {
        auto v1 = R"([ 100 ])"_js;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, "/foo"_jp, 200),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::invalid_array_index);
                BOOST_TEST(e.pointer() == "/foo");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );

        auto expected = R"([ 100 ])"_js;
        BOOST_TEST(v1 == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_with_empty_token_)
    {
        auto v1 = R"([ 100 ])"_js;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, "/"_jp, 200),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::invalid_array_index);
                BOOST_TEST(e.pointer() == "/");
                BOOST_TEST(e.pos() == 1);
                return true;
            } );

        auto expected = R"([ 100 ])"_js;
        BOOST_TEST(v1 == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_object_on_object_with_object_key_1_)
    {
        auto v1 = R"({ "foo": 100 })"_js;
        auto v2 = R"({ "bar": 200 })"_js;

        json::insert(v1, "/baz"_jp, v2);

        auto expected = R"({
          "foo": 100,
          "baz": {
             "bar": 200
          }
        })"_js;

        BOOST_TEST(v1 == expected);
    }

    BOOST_AUTO_TEST_CASE(insert_object_on_object_with_object_key_3_)
    {
        auto v1 = R"({ "a": { "b": { "c": { "d": {} } } } })"_js;

        auto& v2 = json::insert(v1, "/a/b/c/d/e"_jp, 100);

        auto expected = R"({ "a": { "b": { "c": { "d": { "e": 100 } } } } })"_js;

        BOOST_TEST(v1 == expected);

        v2 = 200;
        BOOST_TEST(v1 == R"({ "a": { "b": { "c": { "d": { "e": 200 } } } } })"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_object_on_array_)
    {
        auto v1 = R"({
          "foo": 100,
          "baz": [ "hello" ]
        })"_js;
        auto v2 = R"({ "bar": 200 })"_js;

        json::insert(v1, "/baz/-"_jp, v2);

        auto expected = R"({
          "foo": 100,
          "baz": [
            "hello", {
              "bar": 200
            }
          ]
        })"_js;

        BOOST_TEST(v1 == expected);
    }

    BOOST_AUTO_TEST_CASE(lvalue_on_lvalue_object_)
    {
        auto v1 = R"({})"_js;
        auto v2 = R"("xxx")"_js;

        auto& v3 = json::insert(v1.get_object(), "/foo"_jp, v2);
        BOOST_TEST(v1 == R"({ "foo": "xxx" })"_js);

        v3 = 100;
        BOOST_TEST(v1 == R"({ "foo": 100 })"_js);
    }

    BOOST_AUTO_TEST_CASE(rvalue_on_lvalue_object_)
    {
        auto v1 = R"({})"_js;
        auto v2 = R"("xxx")"_js;

        auto& v3 = json::insert(v1.get_object(), "/foo"_jp,
                                static_cast<json::value&&>(std::move(v2)));

        BOOST_TEST(v1 == R"({ "foo": "xxx" })"_js);

        v3 = 200;
        BOOST_TEST(v1 == R"({ "foo": 200 })"_js);
    }

    BOOST_AUTO_TEST_CASE(lvalue_on_lvalue_array_)
    {
        auto v1 = R"([ 100 ])"_js;
        auto v2 = R"("xxx")"_js;

        auto& v3 = json::insert(v1.get_array(), "/-"_jp, v2);

        BOOST_TEST(v1 == R"([ 100, "xxx" ])"_js);

        v3 = 300;
        BOOST_TEST(v1 == R"([ 100, 300 ])"_js);
    }

    BOOST_AUTO_TEST_CASE(rvalue_on_lvalue_array_)
    {
        auto v1 = R"([ 100 ])"_js;
        auto v2 = R"("xxx")"_js;

        auto& v3 = json::insert(v1.get_array(), "/-"_jp,
                                static_cast<json::value&&>(std::move(v2)));

        BOOST_TEST(v1 == R"([ 100, "xxx" ])"_js);

        v3 = 300;
        BOOST_TEST(v1 == R"([ 100, 300 ])"_js);
    }

    BOOST_AUTO_TEST_CASE(fill_empty_array_1_)
    {
        auto v1 = R"([])"_js;

        auto& v3 = json::insert(v1, "/3"_jp, 1, {
            .create_empty_value_on_array = true
        });

        BOOST_TEST(v1 == R"([null, null, null, 1])"_js);

        v3 = 2;
        BOOST_TEST(v1 == R"([null, null, null, 2])"_js);
    }

    BOOST_AUTO_TEST_CASE(fill_empty_array_2_)
    {
        auto v1 = R"(null)"_js;

        auto& v3 = json::insert(v1, "/1"_jp, 1, {
            .create_empty_value_on_array = true
        });

        BOOST_TEST(v1 == R"([null, 1])"_js);

        v3 = 2;
        BOOST_TEST(v1 == R"([null, 2])"_js);
    }

    BOOST_AUTO_TEST_CASE(regard_last_token_as_object_key_1_)
    {
        auto v1 = R"({})"_js;

        auto& v2 = json::insert(v1, "/0"_jp, 1, {
            .regard_last_token_of_path_as_object_key = true
        });

        BOOST_TEST(v1 == R"({ "0": 1 })"_js);

        v2 = 2;
        BOOST_TEST(v1 == R"({ "0": 2 })"_js);
    }

    BOOST_AUTO_TEST_CASE(regard_last_token_as_object_key_2_)
    {
        auto v1 = R"([])"_js;

        BOOST_CHECK_EXCEPTION(
            json::insert(v1, "/0"_jp, 1, {
                .regard_last_token_of_path_as_object_key = true
            }),
            json::pointer::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/0",
                    "pos": 1
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // insert_

BOOST_AUTO_TEST_SUITE(insert_or_replace_)

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        auto v1 = R"({ "a": 1 })"_js;
        auto const v2 = R"([ 1, 2 ])"_js;

        auto& rv = json::insert_or_replace(v1, ""_jp, v2);

        BOOST_TEST(v1 == R"([ 1, 2 ])"_js);

        rv = 100;
        BOOST_TEST(v1 == 100);
    }

    BOOST_AUTO_TEST_CASE(object_insert_1_)
    {
        auto v1 = R"({})"_js;

        auto& rv = json::insert_or_replace(v1, "/"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"({"":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_replace_1_)
    {
        auto v1 = R"({"":null})"_js;

        auto& rv = json::insert_or_replace(v1, "/"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"({"":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(array_insert_1_)
    {
        auto v1 = R"([])"_js;

        auto& rv = json::insert_or_replace(v1, "/0"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"([1])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_insert_2_)
    {
        auto v1 = R"([0])"_js;

        auto& rv = json::insert_or_replace(v1, "/0"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"([1,0])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_insert_3_)
    {
        auto v1 = R"([0])"_js;

        auto& rv = json::insert_or_replace(v1, "/-"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"([0,1])"_js);
    }

BOOST_AUTO_TEST_SUITE_END() // insert_or_replace

} // namespace testing
