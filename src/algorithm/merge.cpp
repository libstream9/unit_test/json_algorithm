#include <stream9/json/algorithm/merge.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(merge_)

    BOOST_AUTO_TEST_CASE(null_with_null_)
    {
        json::value v1, v2;

        auto rv = json::merge(v1, v2);

        BOOST_TEST(rv.is_null());
    }

    BOOST_AUTO_TEST_CASE(empty_object_with_empty_object_)
    {
        auto v1 = R"({})"_js;
        auto v2 = R"({})"_js;

        auto rv = json::merge(v1, v2);

        BOOST_TEST(rv == R"({})"_js);
    }

    BOOST_AUTO_TEST_CASE(scalar_into_new_key_)
    {
        auto v1 = R"({})"_js;
        auto v2 = R"({ "a": 0, "b": [], "c": {} })"_js;

        auto rv = json::merge(v1, v2);

        BOOST_TEST(rv == R"({ "a": 0, "b": [], "c": {} })"_js);
    }

    BOOST_AUTO_TEST_CASE(non_object_into_existent_key_with_scalar_value_)
    {
        auto v1 = R"({ "a": 0, "b": 0 })"_js;
        auto v2 = R"({ "a": 1 })"_js;

        auto rv = json::merge(v1, v2);

        BOOST_TEST(rv == R"({ "a": 1, "b": 0 })"_js);
    }

    BOOST_AUTO_TEST_CASE(non_object_into_existent_key_with_object_value_)
    {
        auto v1 = R"({ "a": {}, "b": {} })"_js;
        auto v2 = R"({ "a": 1 })"_js;

        auto rv = json::merge(v1, v2);

        BOOST_TEST(rv == R"({ "a": 1, "b": {} })"_js);
    }

    BOOST_AUTO_TEST_CASE(object_into_existent_key_with_object_value_)
    {
        auto v1 = R"({ "a": {}, "b": {} })"_js;
        auto v2 = R"({ "a": { "x": 1 } })"_js;

        auto rv = json::merge(v1, v2);

        BOOST_TEST(rv == R"({ "a": { "x": 1 }, "b": {} })"_js);
    }

    BOOST_AUTO_TEST_CASE(null_with_new_key_)
    {
        auto v1 = R"({})"_js;
        auto v2 = R"({ "a": null })"_js;

        auto rv = json::merge(v1, v2);

        BOOST_TEST(rv == R"({})"_js);
    }

    BOOST_AUTO_TEST_CASE(null_with_existent_key_1_)
    {
        auto v1 = R"({ "a": 1, "b": 2 })"_js;
        auto v2 = R"({ "a": null })"_js;

        auto rv = json::merge(v1, v2);

        BOOST_TEST(rv == R"({ "b": 2 })"_js);
    }

    BOOST_AUTO_TEST_CASE(null_with_existent_key_2_)
    {
        auto v1 = R"({ "a": { "x": { "y": { "z": 1 } } }, "b": 2 })"_js;
        auto v2 = R"({ "a": { "x": null } })"_js;

        auto rv = json::merge(v1, v2);

        BOOST_TEST(rv == R"({ "a": {}, "b": 2 })"_js);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_1_)
    {
        auto v1 = R"({
          "title": "Goodbye!",
          "author": {
            "givenName": "John",
            "familyName": "Doe"
          },
          "tags": [ "example", "sample" ],
          "content": "This will be unchanged"
        })"_js;

        auto v2 = R"({
          "title": "Hello!",
          "phoneNumber": "+01-123-456-7890",
          "author": {
            "familyName": null
          },
          "tags": [ "example" ]
        })"_js;

        auto rv = json::merge(v1, v2);

        auto expected = R"({
          "title": "Hello!",
          "author": {
            "givenName" : "John"
          },
          "tags": [ "example" ],
          "content": "This will be unchanged",
          "phoneNumber": "+01-123-456-7890"
        })"_js;

        BOOST_TEST(rv == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_2_)
    {
        auto v1 = R"({"a":"b"})"_js;
        auto v2 = R"({"a":"c"})"_js;
        auto expected = R"({"a":"c"})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_3_)
    {
        auto v1 = R"({"a":"b"})"_js;
        auto v2 = R"({"b":"c"})"_js;
        auto expected = R"({"a":"b", "b":"c"})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_4_)
    {
        auto v1 = R"({"a":"b"})"_js;
        auto v2 = R"({"a":null})"_js;
        auto expected = R"({})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_5_)
    {
        auto v1 = R"({"a":"b", "b":"c"})"_js;
        auto v2 = R"({"a":null})"_js;
        auto expected = R"({"b":"c"})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_6_)
    {
        auto v1 = R"({"a":["b"]})"_js;
        auto v2 = R"({"a":"c"})"_js;
        auto expected = R"({"a":"c"})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_7_)
    {
        auto v1 = R"({"a":"c"})"_js;
        auto v2 = R"({"a":["b"]})"_js;
        auto expected = R"({"a":["b"]})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_8_)
    {
        auto v1 = R"({"a":{"b":"c"}})"_js;
        auto v2 = R"({"a":{"b":"d","c":null}})"_js;
        auto expected = R"({"a":{"b":"d"}})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_9_)
    {
        auto v1 = R"({"a":[{"b":"c"}]})"_js;
        auto v2 = R"({"a":[1]})"_js;
        auto expected = R"({"a":[1]})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_10_)
    {
        auto v1 = R"(["a","b"])"_js;
        auto v2 = R"(["c","d"])"_js;
        auto expected = R"(["c","d"])"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_11_)
    {
        auto v1 = R"({"a":"b"})"_js;
        auto v2 = R"(["c"])"_js;
        auto expected = R"(["c"])"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_12_)
    {
        auto v1 = R"({"a":"foo"})"_js;
        auto v2 = R"(null)"_js;
        auto expected = R"(null)"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_13_)
    {
        auto v1 = R"({"a":"foo"})"_js;
        auto v2 = R"("bar")"_js;
        auto expected = R"("bar")"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_14_)
    {
        auto v1 = R"({"e":null})"_js;
        auto v2 = R"({"a":1})"_js;
        auto expected = R"({"e":null,"a":1})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_15_)
    {
        auto v1 = R"([1,2])"_js;
        auto v2 = R"({"a":"b","c":null})"_js;
        auto expected = R"({"a":"b"})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

    BOOST_AUTO_TEST_CASE(rfc7386_example_16_)
    {
        auto v1 = R"({})"_js;
        auto v2 = R"({"a":{"bb":{"ccc":null}}})"_js;
        auto expected = R"({"a":{"bb":{}}})"_js;

        BOOST_TEST(json::merge(v1, v2) == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // merge_

} // namespace testing
