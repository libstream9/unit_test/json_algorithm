#include <stream9/json/algorithm/walk.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace json::literals;

using E = json::pointer::errc;

BOOST_AUTO_TEST_SUITE(walk_)

    BOOST_AUTO_TEST_CASE(empty_pointer_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, ""_jp);

        auto expected = v1;

        BOOST_TEST(ec == E::ok);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 0);
    }

    BOOST_AUTO_TEST_CASE(empty_pointer_token_on_object_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, "/foo/////baz/-"_jp);

        auto expected = R"({ "bar": 200, "baz": [100, 200, 300] })"_js;

        BOOST_TEST(ec == E::object_key_does_not_exist);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 1);
    }

    BOOST_AUTO_TEST_CASE(empty_pointer_token_on_array_)
    {
        auto v1 = R"({
          "foo": {
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, "/foo/baz/"_jp);

        auto expected = R"([100, 200, 300])"_js;

        BOOST_TEST(ec == E::invalid_array_index);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 2);
    }

    BOOST_AUTO_TEST_CASE(object_object_array_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, "/foo/baz/1"_jp);
        static_assert(std::same_as<decltype(v2), json::value&>);

        auto expected = 200;

        BOOST_TEST(ec == E::ok);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 3);
    }

    BOOST_AUTO_TEST_CASE(array_index_with_leading_zero_1_)
    {
        using E = json::pointer::errc;

        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto rv = json::walk(v1, "/foo/baz/01"_jp);

        BOOST_TEST(rv.ec == E::array_index_has_leading_zero);
        BOOST_TEST(rv.value == R"([ 100, 200, 300 ])"_js);
        BOOST_TEST(rv.n_step == 2);
    }

    BOOST_AUTO_TEST_CASE(array_index_with_leading_zero_2_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "04": 100
          }
        })"_js;

        auto rv = json::walk(v1, "/foo/04"_jp);

        BOOST_TEST(rv.ec == E::ok);
        BOOST_TEST(rv.value == R"(100)"_js);
        BOOST_TEST(rv.n_step == 2);
    }

    BOOST_AUTO_TEST_CASE(object_key_does_not_exist_1_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, "/bar"_jp);

        auto expected = v1;

        BOOST_TEST(ec == E::object_key_does_not_exist);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 0);
    }

    BOOST_AUTO_TEST_CASE(object_key_does_not_exist_2_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, "/foo/xyxxy"_jp);

        auto expected = R"({
          "bar": 200,
          "baz": [100, 200, 300]
        })"_js;

        BOOST_TEST(ec == E::object_key_does_not_exist);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 1);
    }

    BOOST_AUTO_TEST_CASE(invalid_array_index_1_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, "/foo/baz/foo"_jp);

        auto expected = R"([100, 200, 300])"_js;

        BOOST_TEST(ec == E::invalid_array_index);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 2);
    }

    BOOST_AUTO_TEST_CASE(invalid_array_index_2_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, "/foo/baz/10000000000000000000000000"_jp);

        auto expected = R"([100, 200, 300])"_js;

        BOOST_TEST(ec == E::invalid_array_index);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 2);
    }

    BOOST_AUTO_TEST_CASE(array_index_out_of_bound_1_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, "/foo/baz/200"_jp);

        auto expected = R"([100, 200, 300])"_js;

        BOOST_TEST(ec == E::array_index_out_of_bound);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 2);
    }

    BOOST_AUTO_TEST_CASE(array_index_out_of_bound_2_)
    {
        auto v1 = R"({
          "foo": {
            "bar": 200,
            "baz": [100, 200, 300]
          }
        })"_js;

        auto [ec, v2, n] = json::walk(v1, "/foo/baz/-"_jp);

        auto expected = R"([100, 200, 300])"_js;

        BOOST_TEST(ec == E::array_index_out_of_bound);
        BOOST_TEST(v2 == expected);
        BOOST_TEST(n == 2);
    }

BOOST_AUTO_TEST_SUITE_END() // walk_

} // namespace testing
