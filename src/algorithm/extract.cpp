#include <stream9/json/algorithm/extract.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace stream9::json::literals;

BOOST_AUTO_TEST_SUITE(extract_)

BOOST_AUTO_TEST_CASE(empty_)
{
    json::value v1;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, ""_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "extract with empty pointer",
                "pointer": "",
                "pos": 0
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(from_object_)
{
    auto v1 = R"({ "a": 1 })"_js;

    auto rv = json::extract(v1, "/a"_jp);

    BOOST_TEST(v1 == R"({})"_js);
    BOOST_TEST(rv == 1);
}

BOOST_AUTO_TEST_CASE(object_key_on_null_)
{
    json::value v1;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, "/a"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "object key on null",
                "pointer": "/a",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(object_key_on_scalar_)
{
    json::value v1 = 1;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, "/a"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "object key on scalar",
                "pointer": "/a",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(no_path_to_parent_)
{
    auto v1 = R"({ "a": { "b": 1 } })"_js;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, "/b"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "object key does not exist",
                "pointer": "/b",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(no_path_to_target_)
{
    auto v1 = R"({ "a": 1 })"_js;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, "/b"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "object key does not exist",
                "pointer": "/b",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(from_array_)
{
    auto v1 = R"([ 1, 2 ])"_js;

    auto rv = json::extract(v1, "/1"_jp);

    BOOST_TEST(v1 == R"([ 1 ])"_js);
    BOOST_TEST(rv == 2);
}

BOOST_AUTO_TEST_CASE(array_index_on_null_)
{
    json::value v1;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, "/0"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "array index on null",
                "pointer": "/0",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(array_index_on_scalar_)
{
    json::value v1 = 1;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, "/0"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "array index on scalar",
                "pointer": "/0",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(array_index_out_of_bound_1_)
{
    auto v1 = R"([ 1 ])"_js;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, "/1"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "array index out of bound",
                "pointer": "/1",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(array_index_out_of_bound_2_)
{
    auto v1 = R"([ 1 ])"_js;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, "/2"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "array index out of bound",
                "pointer": "/2",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(invalid_array_index_)
{
    auto v1 = R"([ 1 ])"_js;

    BOOST_CHECK_EXCEPTION(
        json::extract(v1, "/00"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "array index has leading zero",
                "pointer": "/00",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_SUITE_END() // extract_

} // namespace testing
