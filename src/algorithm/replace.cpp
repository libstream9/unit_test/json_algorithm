#include <stream9/json/algorithm/replace.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace json::literals;
using E = json::pointer::errc;

BOOST_AUTO_TEST_SUITE(replace_)

    BOOST_AUTO_TEST_CASE(empty_value_with_empty_value_at_empty_pointer_)
    {
        json::value v1, v2;

        auto& rv = json::replace(v1, ""_jp, v2);

        BOOST_TEST(&rv == &v1);
        BOOST_TEST(rv.is_null());
    }

    BOOST_AUTO_TEST_CASE(empty_value_with_scalar_at_empty_pointer_)
    {
        json::value v1;

        auto& rv = json::replace(v1, ""_jp, 1);

        BOOST_TEST(&rv == &v1);
        BOOST_TEST(rv == 1);
    }

    BOOST_AUTO_TEST_CASE(replace_value_inside_object_1_)
    {
        auto v1 = R"({"":null})"_js;

        auto& rv = json::replace(v1, "/"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"({"":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(replace_value_inside_object_2_)
    {
        auto v1 = R"({"foo":null})"_js;

        auto& rv = json::replace(v1, "/foo"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"({"foo":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(replace_value_inside_object_3_)
    {
        auto v1 = R"({"foo":{"bar":null}})"_js;

        auto& rv = json::replace(v1, "/foo/bar"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"({"foo":{"bar":1}})"_js);
    }

    BOOST_AUTO_TEST_CASE(replace_value_inside_object_4_)
    {
        auto v1 = R"([{"foo":null}])"_js;

        auto& rv = json::replace(v1, "/0/foo"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"([{"foo":1}])"_js);
    }

    BOOST_AUTO_TEST_CASE(target_value_doesnt_exist_in_object_)
    {
        auto v1 = R"({"bar":null})"_js;

        BOOST_CHECK_EXCEPTION(
            json::replace(v1, "/foo"_jp, 1),
            json::pointer::error,
            [&](auto&& e) {
                BOOST_TEST(e.code() == E::object_key_does_not_exist);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(value_inside_array_1_)
    {
        auto v1 = R"([1, 2])"_js;
        json::value v2;

        auto& rv = json::replace(v1, "/1"_jp, v2);

        BOOST_TEST(rv.is_null());
        BOOST_TEST(v1 == R"([1, null])"_js);
    }

    BOOST_AUTO_TEST_CASE(value_inside_array_2_)
    {
        auto v1 = R"([1,[2]])"_js;

        auto& rv = json::replace(v1, "/1/0"_jp, 5);

        BOOST_TEST(rv == 5);
        BOOST_TEST(v1 == R"([1,[5]])"_js);
    }

    BOOST_AUTO_TEST_CASE(value_inside_array_3_)
    {
        auto v1 = R"({"foo":[1,2]})"_js;

        auto& rv = json::replace(v1, "/foo/1"_jp, 5);

        BOOST_TEST(rv == 5);
        BOOST_TEST(v1 == R"({"foo":[1,5]})"_js);
    }

    BOOST_AUTO_TEST_CASE(invalid_array_index_)
    {
        auto v1 = R"([1, 2])"_js;

        BOOST_CHECK_EXCEPTION(
            json::replace(v1, "/foo"_jp, 1),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::invalid_array_index);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_index_out_of_bound_1_)
    {
        auto v1 = R"([1, 2])"_js;

        BOOST_CHECK_EXCEPTION(
            json::replace(v1, "/2"_jp, 1),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_index_out_of_bound_2_)
    {
        auto v1 = R"([1, 2])"_js;

        BOOST_CHECK_EXCEPTION(
            json::replace(v1, "/-"_jp, 1),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_path_1_)
    {
        auto v1 = R"({})"_js;

        BOOST_CHECK_EXCEPTION(
            json::replace(v1, "/foo"_jp, 1),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::object_key_does_not_exist);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_path_2_)
    {
        auto v1 = R"({"foo":{"bar":null}})"_js;

        BOOST_CHECK_EXCEPTION(
            json::replace(v1, "/foo/foo"_jp, 1),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::object_key_does_not_exist);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_path_3_)
    {
        auto v1 = R"([])"_js;

        BOOST_CHECK_EXCEPTION(
            json::replace(v1, "/0"_jp, 1),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_path_4_)
    {
        auto v1 = R"([1,2])"_js;

        BOOST_CHECK_EXCEPTION(
            json::replace(v1, "/-"_jp, 1),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_path_5_)
    {
        auto v1 = R"([1,[]])"_js;

        BOOST_CHECK_EXCEPTION(
            json::replace(v1, "/1/0"_jp, 1),
            json::pointer::error,
            [](auto&& e) {
                BOOST_TEST(e.code() == E::array_index_out_of_bound);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // replace_

BOOST_AUTO_TEST_SUITE(replace_or_insert_)

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        json::value v1, v2;

        auto& rv = json::replace_or_insert(v1, ""_jp, v2);

        BOOST_TEST(&rv == &v1);
        BOOST_TEST(v1.is_null());
    }

    BOOST_AUTO_TEST_CASE(object_replace_1_)
    {
        auto v1 = R"({"":null})"_js;

        auto& rv = json::replace_or_insert(v1, "/"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"({"":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_insert_1_)
    {
        auto v1 = R"({})"_js;

        auto& rv = json::replace_or_insert(v1, "/"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"({"":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(array_replace_1_)
    {
        auto v1 = R"([0,2])"_js;

        auto& rv = json::replace_or_insert(v1, "/0"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"([1,2])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_insert_1_)
    {
        auto v1 = R"([0,2])"_js;

        auto& rv = json::replace_or_insert(v1, "/-"_jp, 1);

        BOOST_TEST(rv == 1);
        BOOST_TEST(v1 == R"([0,2,1])"_js);
    }

BOOST_AUTO_TEST_SUITE_END() // replace_or_insert_

} // namespace testing
