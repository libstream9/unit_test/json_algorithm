#include <stream9/json/algorithm/move.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace json::literals;

BOOST_AUTO_TEST_SUITE(move_)

BOOST_AUTO_TEST_CASE(empty_src_)
{
    auto v1 = R"(null)"_js;
    auto v2 = R"(null)"_js;

    BOOST_CHECK_EXCEPTION(
        json::move(v1, ""_jp, v2, ""_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "move from empty pointer",
                "pointer": "",
                "pos": 0
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(empty_dest_)
{
    auto v1 = R"({ "a": 0 })"_js;
    auto v2 = R"(null)"_js;

    BOOST_CHECK_EXCEPTION(
        json::move(v1, "/a"_jp, v2, ""_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "move to empty pointer",
                "pointer": "",
                "pos": 0
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(move_to_descendant_)
{
    auto v1 = R"({ "a": 0 })"_js;

    BOOST_CHECK_EXCEPTION(
        json::move(v1, "/a"_jp, v1, "/a/b"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "move source is ancestor of destination",
                "pointer": "/a",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );
}

BOOST_AUTO_TEST_CASE(to_same_place_)
{
    auto v1 = R"({ "a": 0 })"_js;

    auto& rv = json::move(v1, "/a"_jp, v1, "/a"_jp);

    BOOST_TEST(v1 == R"({ "a": 0 })"_js);

    rv = 5;
    BOOST_TEST(v1 == R"({ "a": 5 })"_js);
}

BOOST_AUTO_TEST_CASE(to_array_)
{
    auto v1 = R"({ "a": 0 })"_js;
    auto v2 = R"([ 1, 2 ])"_js;

    auto& rv = json::move(v1, "/a"_jp, v2, "/1"_jp);

    BOOST_TEST(v1 == R"({})"_js);
    BOOST_TEST(v2 == R"([ 1, 0, 2 ])"_js);

    rv = 5;
    BOOST_TEST(v1 == R"({})"_js);
    BOOST_TEST(v2 == R"([ 1, 5, 2 ])"_js);
}

BOOST_AUTO_TEST_CASE(to_object_)
{
    auto v1 = R"([ 1, 2 ])"_js;
    auto v2 = R"({ "a": 0 })"_js;

    auto& rv = json::move(v1, "/0"_jp, v2, "/b"_jp);

    BOOST_TEST(v1 == R"([ 2 ])"_js);
    BOOST_TEST(v2 == R"({ "a": 0, "b": 1 })"_js);

    rv = 5;
    BOOST_TEST(v1 == R"([ 2 ])"_js);
    BOOST_TEST(v2 == R"({ "a": 0, "b": 5 })"_js);
}

BOOST_AUTO_TEST_CASE(to_object_with_existing_key_)
{
    auto v1 = R"([ 1, 2 ])"_js;
    auto v2 = R"({ "a": 0 })"_js;

    BOOST_CHECK_EXCEPTION(
        json::move(v1, "/0"_jp, v2, "/a"_jp),
        json::pointer::error,
        [](auto&& e) {
            auto expected = R"({
                "type": "stream9::json::pointer::error",
                "code": "object key already exist",
                "pointer": "/a",
                "pos": 1
            })"_js;

            BOOST_TEST(json::value_from(e) == expected);
            return true;
        } );

    BOOST_TEST(v1 == R"([ 1, 2 ])"_js);
    BOOST_TEST(v2 == R"({ "a": 0 })"_js);
}

BOOST_AUTO_TEST_CASE(to_object_replace_with_existing_key_)
{
    auto v1 = R"([ 1, 2 ])"_js;
    auto v2 = R"({ "a": 0 })"_js;

    auto& rv = json::move(v1, "/0"_jp, v2, "/a"_jp,
        json::move_method::insert_or_replace );

    BOOST_TEST(v1 == R"([ 2 ])"_js);
    BOOST_TEST(v2 == R"({ "a": 1 })"_js);

    rv = 5;
    BOOST_TEST(v1 == R"([ 2 ])"_js);
    BOOST_TEST(v2 == R"({ "a": 5 })"_js);
}

BOOST_AUTO_TEST_CASE(array_swap_position_)
{
    auto v1 = R"([ 1, 2 ])"_js;

    auto& rv = json::move(v1, "/0"_jp, v1, "/1"_jp);

    BOOST_TEST(v1 == R"([ 2, 1 ])"_js);

    rv = 9;
    BOOST_TEST(v1 == R"([ 2, 9 ])"_js);
}

BOOST_AUTO_TEST_CASE(to_null_)
{
    auto v1 = R"({ "a": 0 })"_js;
    auto v2 = R"(null)"_js;

    auto& rv = json::move(v1, "/a"_jp, v2, "/b"_jp);

    BOOST_TEST(v1 == R"({})"_js);
    BOOST_TEST(v2 == R"({ "b": 0 })"_js);

    rv = 5;
    BOOST_TEST(v1 == R"({})"_js);
    BOOST_TEST(v2 == R"({ "b": 5 })"_js);
}

BOOST_AUTO_TEST_SUITE_END() // move_

} // namespace testing
