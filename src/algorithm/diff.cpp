#include <stream9/json/algorithm/diff.hpp>

#include "../namespace.hpp"

#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <stream9/ranges/range_facade.hpp>
#include <stream9/strings/stream.hpp>

namespace testing {

inline constexpr boost::test_tools::per_element per_element;

class event_recorder : public json::diff_event_handler
                     , public rng::range_facade<event_recorder>
{
public:
    void add(json::pointer const& p, json::value const& v) override
    {
        using str::operator<<;

        std::string ev = "add:";
        ev << p << ":" << v;

        m_events.push_back(std::move(ev));
    }

    void remove(json::pointer const& p, json::value const& v) override
    {
        using str::operator<<;

        std::string ev = "remove:";
        ev << p << ":" << v;

        m_events.push_back(std::move(ev));
    }

    void replace(json::pointer const& p,
                 json::value const& from, json::value const& to) override
    {
        using str::operator<<;

        std::string ev = "replace:";
        ev << p << ":" << from << ":" << to;

        m_events.push_back(std::move(ev));
    }

    void move(json::pointer const& from,
              json::value const& v, json::pointer const& to)
    {
        using str::operator<<;

        std::string ev = "move:";
        ev << from << ":" << v << ":" << to;

        m_events.push_back(std::move(ev));
    }

    auto begin() const { return m_events.begin(); }
    auto end() const { return m_events.end(); }

    friend std::ostream& operator<<(std::ostream& os, event_recorder const& r)
    {
        for (auto&& e: r) {
            os << e << "\n";
        }

        return os;
    }

private:
    std::vector<std::string> m_events;
};

BOOST_AUTO_TEST_SUITE(diff_)

BOOST_AUTO_TEST_CASE(null_vs_null_)
{
    event_recorder r;

    json::diff("null"_js, "null"_js, r);

    BOOST_TEST(r.empty());
}

BOOST_AUTO_TEST_CASE(scalar_vs_null_)
{
    event_recorder r;

    json::diff("1"_js, "null"_js, r);

    auto const expected = {
        "replace::1:null",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(null_vs_scalar_)
{
    event_recorder r;

    json::diff("null"_js, "1"_js, r);

    auto const expected = {
        "replace::null:1",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(scalar_vs_scalar_)
{
    event_recorder r;

    json::diff("1"_js, R"("foo")"_js, r);

    auto const expected = {
        R"(replace::1:"foo")"
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(null_vs_object_)
{
    event_recorder r;

    json::diff("null"_js, R"({"foo":1})"_js, r);

    auto const expected = {
        R"(replace::null:{"foo":1})"
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_1_)
{
    event_recorder r;

    json::diff("{}"_js, R"({"foo":1})"_js, r);

    auto const expected = {
        R"(add:/foo:1)"
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_2_)
{
    event_recorder r;

    json::diff(R"({"foo":1})"_js, "{}"_js, r);

    auto const expected = {
        R"(remove:/foo:1)"
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_3_)
{
    event_recorder r;

    json::diff(R"({"foo":1})"_js, R"({"foo":2})"_js, r);

    auto const expected = {
        R"(replace:/foo:1:2)"
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_4_)
{
    event_recorder r;

    json::diff(R"({"foo":1})"_js, R"({"bar":1})"_js, r);

    auto const expected = {
        R"(move:/foo:1:/bar)"
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_5_)
{
    event_recorder r;

    json::diff(R"({"foo":1})"_js, R"({"bar":2})"_js, r);

    auto const expected = {
        R"(remove:/foo:1)",
        R"(add:/bar:2)",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_6_)
{
    event_recorder r;

    json::diff(R"({"foo":{}})"_js, R"({"foo":{"abc":1}})"_js, r);

    auto const expected = {
        R"(add:/foo/abc:1)",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_7_)
{
    event_recorder r;

    json::diff(R"({"foo":{"abc":1}})"_js, R"({"foo":{}})"_js, r);

    auto const expected = {
        R"(remove:/foo/abc:1)",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_8_)
{
    event_recorder r;

    json::diff(R"({"foo":{"abc":1}})"_js, R"({"foo":{"abc":2}})"_js, r);

    auto const expected = {
        R"(replace:/foo/abc:1:2)",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_9_)
{
    event_recorder r;

    json::diff(R"({"foo":{"abc":1}})"_js, R"({"foo":{"xyz":1}})"_js, r);

    auto const expected = {
        R"(move:/foo/abc:1:/foo/xyz)"
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_10_)
{
    event_recorder r;

    json::diff(R"({"foo":{"abc":1}})"_js, R"({"foo":{"xyz":2}})"_js, r);

    auto const expected = {
        R"(remove:/foo/abc:1)",
        R"(add:/foo/xyz:2)",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_11_)
{
    event_recorder r;

    auto const& v1 = R"({
        "foo": { "x": "abc" },
        "bar": {},
        "baz": {}
    })"_js;

    auto const& v2 = R"({
        "foo": {},
        "bar": {},
        "baz": { "y": "abc" }
    })"_js;

    json::diff(v1, v2, r);

    auto const expected = {
        R"(add:/baz/y:"abc")",
        R"(remove:/foo/x:"abc")",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_12_)
{
    event_recorder r;

    auto const& v1 = R"({
        "foo": { "x": "abc" },
        "bar": {},
        "baz": {}
    })"_js;

    auto const& v2 = R"({
        "foo": {},
        "bar": {},
        "baz": { "x": "abc" }
    })"_js;

    json::diff(v1, v2, r);

    auto const expected = {
        R"(add:/baz/x:"abc")",
        R"(remove:/foo/x:"abc")",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(object_vs_object_13_)
{
    event_recorder r;

    auto const& v1 = R"({
        "foo": { "x": "abc" },
        "bar": {},
        "baz": {}
    })"_js;

    auto const& v2 = R"({
        "bar": {},
        "baz": {},
        "xyzzy": { "x": "abc" }
    })"_js;

    json::diff(v1, v2, r);

    auto const expected = {
        R"(move:/foo:{"x":"abc"}:/xyzzy)",
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_SUITE(array_vs_array_)

    BOOST_AUTO_TEST_CASE(empty_vs_empty_)
    {
        event_recorder r;

        json::diff(R"([])"_js, R"([])"_js, r);

        BOOST_TEST(r.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_vs_one_scalar_)
    {
        event_recorder r;

        json::diff(R"([])"_js, R"([1])"_js, r);

        auto const expected = {
            R"(add:/0:1)",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(one_scalar_vs_empty_)
    {
        event_recorder r;

        json::diff(R"([1])"_js, R"([])"_js, r);

        auto const expected = {
            R"(remove:/0:1)",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(one_scalar_vs_one_scalar_)
    {
        event_recorder r;

        json::diff(R"([1])"_js, R"(["foo"])"_js, r);

        auto const expected = {
            R"(replace:/0:1:"foo")"
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(one_scalar_vs_two_scalar_1_)
    {
        event_recorder r;

        json::diff(R"([1])"_js, R"([1, 2])"_js, r);

        auto const expected = {
            R"(add:/1:2)"
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(one_scalar_vs_two_scalar_2_)
    {
        event_recorder r;

        json::diff(R"([1])"_js, R"([2, 1])"_js, r);

        auto const expected = {
            R"(add:/0:2)",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(one_scalar_vs_two_scalar_3_)
    {
        event_recorder r;

        json::diff(R"([1])"_js, R"([2, 3])"_js, r);

        auto const expected = {
            R"(replace:/0:1:2)",
            R"(add:/1:3)",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(move_scalar_1_)
    {
        event_recorder r;

        json::diff(R"([1, 2, 3])"_js, R"([2, 3, 1])"_js, r);

        auto const expected = {
            R"(move:/0:1:/2)",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(move_scalar_2_)
    {
        event_recorder r;

        json::diff(R"([1, 2, 3])"_js, R"([3, 1, 2])"_js, r);

        auto const expected = {
            R"(move:/2:3:/0)",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(scalar_vs_object_)
    {
        event_recorder r;

        json::diff(R"([1,2])"_js, R"([1,{}])"_js, r);

        auto const expected = {
            R"(replace:/1:2:{})",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(object_vs_object_)
    {
        event_recorder r;

        json::diff(R"([1,{},"x"])"_js, R"([2,{"foo":1},"y"])"_js, r);

        auto const expected = {
            R"(replace:/0:1:2)",
            R"(add:/1/foo:1)",
            R"(replace:/2:"x":"y")",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(move_object_1_)
    {
        event_recorder r;

        json::diff(R"([{}, 2, 3])"_js, R"([2, 3, {}])"_js, r);

        auto const expected = {
            R"(move:/0:{}:/2)",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(move_object_2_)
    {
        event_recorder r;

        json::diff(R"([1, 2, {}])"_js, R"([{}, 1, 2])"_js, r);

        auto const expected = {
            R"(move:/2:{}:/0)",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(scalar_vs_array_)
    {
        event_recorder r;

        json::diff(R"([1,2])"_js, R"([1,[]])"_js, r);

        auto const expected = {
            R"(replace:/1:2:[])",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(array_vs_array_)
    {
        event_recorder r;

        json::diff(R"([1,[],"x"])"_js, R"([2,[2],"y"])"_js, r);

        auto const expected = {
            R"(replace:/0:1:2)",
            R"(add:/1/0:2)",
            R"(replace:/2:"x":"y")",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(move_array_1_)
    {
        event_recorder r;

        json::diff(R"([[], 2, 3])"_js, R"([2, 3, []])"_js, r);

        auto const expected = {
            R"(move:/0:[]:/2)",
        };

        BOOST_TEST(r == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(move_array_2_)
    {
        event_recorder r;

        json::diff(R"([1, 2, []])"_js, R"([[], 1, 2])"_js, r);

        auto const expected = {
            R"(move:/2:[]:/0)",
        };

        BOOST_TEST(r == expected, per_element);
    }

BOOST_AUTO_TEST_SUITE_END() // array_vs_array_

BOOST_AUTO_TEST_SUITE_END() // diff_

} // namespace testing
