#include <stream9/json/algorithm/match.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(match_)

    struct mismatch_capturer : json::object
    {
        bool operator()(json::pointer const& p, json::value const& v1, json::value const& v2)
        {
            (*this)[p.to_string()] = json::array { v1, v2 }; //TODO ambiguate with operator=(boost::json::value)
            return false;
        }
    };

    BOOST_AUTO_TEST_SUITE(value_)

        BOOST_AUTO_TEST_CASE(null_1_)
        {
            json::value v1;
            json::value v2;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(null_2_)
        {
            json::value v1;
            json::value v2 = 1;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(null_3_)
        {
            json::value v1;
            json::value v2 = 1;

            mismatch_capturer mismatches;
            auto r1 = json::match(v1, v2, mismatches);

            auto expected = json::object {
                { "", { nullptr, 1 } }
            };

            BOOST_TEST(!r1);
            BOOST_TEST(mismatches == expected);
        }

        BOOST_AUTO_TEST_CASE(bool_1_)
        {
            json::value v1 = true;
            json::value v2 = true;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(bool_2_)
        {
            json::value v1 = true;
            json::value v2 = false;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(bool_3_)
        {
            json::value v1 = true;
            json::value v2 = false;

            mismatch_capturer mismatches;
            auto r1 = json::match(v1, v2, mismatches);

            auto expected = json::object {
                { "", { true, false } }
            };

            BOOST_TEST(!r1);
            BOOST_TEST(mismatches == expected);
        }

        BOOST_AUTO_TEST_CASE(int64_1_)
        {
            json::value v1 = (int64_t)1;
            json::value v2 = (int64_t)1;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(int64_2_)
        {
            json::value v1 = (int64_t)1;
            json::value v2 = (int64_t)2;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(int64_3_)
        {
            json::value v1 = (int64_t)1;
            json::value v2 = (int64_t)2;

            mismatch_capturer mismatches;
            auto r1 = json::match(v1, v2, mismatches);

            auto expected = json::object {
                { "", { 1, 2 } }
            };

            BOOST_TEST(!r1);
            BOOST_TEST(mismatches == expected);
        }

        BOOST_AUTO_TEST_CASE(uint64_1_)
        {
            json::value v1 = (uint64_t)1;
            json::value v2 = (uint64_t)1;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(uint64_2_)
        {
            json::value v1 = (uint64_t)1;
            json::value v2 = (uint64_t)2;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(double_1_)
        {
            json::value v1 = (double)1.0;
            json::value v2 = (double)1.0;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(double_2_)
        {
            json::value v1 = (double)1.0;
            json::value v2 = (double)2.0;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(string_1_)
        {
            json::value v1 = "foo";
            json::value v2 = "foo";

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(string_2_)
        {
            json::value v1 = "foo";
            json::value v2 = "fo"; // substring doesn't count

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "");
        }

    BOOST_AUTO_TEST_SUITE_END() // value_

    BOOST_AUTO_TEST_SUITE(array_)

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            json::array v1 {};
            json::array v2 {};

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            json::array v1 { 1 };
            json::array v2 {};

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(empty_3_)
        {
            json::array v1 {};
            json::array v2 { 1 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/0");
        }

        BOOST_AUTO_TEST_CASE(one_element_1_)
        {
            json::array v1 { 1 };
            json::array v2 { 1 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(one_element_2_)
        {
            json::array v1 { 1 };
            json::array v2 { 2 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/0");
        }

        BOOST_AUTO_TEST_CASE(two_element_1_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 1 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(two_element_2_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 3 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/0");
        }

        BOOST_AUTO_TEST_CASE(two_element_3_)
        {
            json::array v1 { 1,};
            json::array v2 { 1, 2 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/1");
        }

        BOOST_AUTO_TEST_CASE(two_element_4_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 1, 2 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(two_element_5_)
        {
            json::array v1 { 2, 1 };
            json::array v2 { 1, 2 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(two_element_6_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 1, 3 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/1");
        }

        BOOST_AUTO_TEST_CASE(two_element_7_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 4, 3 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/0");
        }

        BOOST_AUTO_TEST_CASE(two_element_8_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 4, 3 };

            mismatch_capturer mismatch;

            auto r1 = json::match(v1, v2, mismatch);

            auto expected = json::object{
                { "/0", { nullptr, 4,} }, //TODO json::null
                { "/1", { nullptr, 3 } },
            };

            BOOST_TEST(!r1);
            BOOST_TEST(mismatch == expected);
        }

        BOOST_AUTO_TEST_CASE(three_element_1_)
        {
            json::array v1 { 1, 2, 3 };
            json::array v2 { 1, 1 };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/1");
        }

    BOOST_AUTO_TEST_SUITE_END() // array_

    BOOST_AUTO_TEST_SUITE(object_)

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            json::object v1;
            json::object v2;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            json::object v1;
            json::object v2 { { "x", 1 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/x");
        }

        BOOST_AUTO_TEST_CASE(empty_3_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2;

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(one_element_1_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "x", 1 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(one_element_2_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "x", 2 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/x");
        }

        BOOST_AUTO_TEST_CASE(one_element_2_1_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "x", 2 } };

            mismatch_capturer mismatches;
            auto r1 = json::match(v1, v2, mismatches);

            auto expected = json::object {
                { "/x", { 1, 2 } },
            };

            BOOST_TEST(!r1);
            BOOST_TEST(mismatches == expected);
        }

        BOOST_AUTO_TEST_CASE(one_element_3_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "y", 1 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/y");
        }

        BOOST_AUTO_TEST_CASE(one_element_3_1_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "y", 1 } };

            mismatch_capturer mismatches;
            auto r1 = json::match(v1, v2, mismatches);

            json::object expected {
                { "/y", { nullptr, 1 } },
            };

            BOOST_TEST(!r1);
            BOOST_TEST(mismatches == expected);
        }

        BOOST_AUTO_TEST_CASE(two_elements_1_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 1 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(two_elements_2_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 2 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/x");
        }

        BOOST_AUTO_TEST_CASE(two_elements_3_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "z", 1 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/z");
        }

        BOOST_AUTO_TEST_CASE(two_elements_4_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "x", 1 }, { "y", 1 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/y");
        }

        BOOST_AUTO_TEST_CASE(two_elements_5_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 1 }, { "y", 1 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(two_elements_6_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 2 }, { "y", 1 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/x");
        }

        BOOST_AUTO_TEST_CASE(two_elements_8_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 1 }, { "z", 1 } };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(!r1);
            BOOST_TEST(r1.mismatch == "/z");
        }

        BOOST_AUTO_TEST_CASE(two_elements_9_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 2 }, { "z", 1 } };

            mismatch_capturer mismatches;
            auto r1 = json::match(v1, v2, mismatches);

            json::object expected {
                { "/x", { 1, 2 } },
                { "/z", { nullptr, 1 } },
            };

            BOOST_TEST(!r1);
            BOOST_TEST(mismatches == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // object_

    BOOST_AUTO_TEST_SUITE(recursive_)

        BOOST_AUTO_TEST_CASE(array_array_1_)
        {
            json::array v1 {
                1,
                { 1, 2, { 1.5, false } },
            };

            json::array v2 {
                { 2, { false } },
            };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(array_array_2_)
        {
            json::array v1 {
                1,
                { 1, 2, { false } },
            };

            json::array v2 {
                { 3, 1, { 1.5 } },
                "foo",
            };

            mismatch_capturer mismatches;
            auto r1 = json::match(v1, v2, mismatches);

            json::object expected {
                { "/0/0",   { nullptr, 3 } },
                { "/0/2/0", { nullptr, 1.5 } },
                { "/1",     { nullptr, "foo" } },
            };

            BOOST_TEST(!r1);
            BOOST_TEST(mismatches == expected);

            auto r2 = json::match(v1, v2);
            BOOST_TEST(!r2);
            BOOST_TEST(r2.mismatch == "/0/0");
        }

        BOOST_AUTO_TEST_CASE(array_object_1_)
        {
            json::array v1 {
                1,
                {
                    { "x", 1 },
                    { "y", 2 },
                },
            };

            json::array v2 {
                {
                    { "y", 2 },
                },
            };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(object_object_1_)
        {
            json::object v1 {
                { "a",
                    {
                        { "x", 1 },
                        { "y", 2 },
                    },
                },
                { "b", 1 },
            };

            json::object v2 {
                { "a",
                    {
                        { "y", 2 },
                    },
                },
            };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

        BOOST_AUTO_TEST_CASE(object_object_2_)
        {
            json::object v1 {
                { "a",
                    {
                        { "x", 1 },
                        { "y", 2 },
                    },
                },
                { "b", 1 },
                { "d", 1 },
                { "e", { { "x", 1 } } },
            };

            json::object v2 {
                { "a",
                    {
                        { "y", 3 },
                        { "z", "foo" },
                    },
                },
                { "b", 2 },
                { "c", 1 },
                { "d", true },
                { "e", "bar" },
            };

            mismatch_capturer mismatches;
            auto r1 = json::match(v1, v2, mismatches);

            json::object expected {
                { "/a/y", { 2, 3 } },
                { "/a/z", { nullptr, "foo" } },
                { "/b",   { 1, 2 } },
                { "/c",   { nullptr, 1 } },
                { "/d",   { 1, true } },
                { "/e",   { { { "x", 1 } } , "bar" } },
            };

            BOOST_TEST(!r1);
            BOOST_TEST(mismatches == expected);

            auto r2 = json::match(v1, v2);

            BOOST_TEST(!r2);
            BOOST_TEST(r2.mismatch == "/a/y");
        }

        BOOST_AUTO_TEST_CASE(object_array_1_)
        {
            json::object v1 {
                { "a", { 1, 2 } },
                { "b", 1 },
            };

            json::object v2 {
                { "a", { 2 } },
            };

            auto r1 = json::match(v1, v2);

            BOOST_TEST(r1);
            BOOST_TEST(r1.mismatch == "");
        }

    BOOST_AUTO_TEST_SUITE_END() // recursive_

BOOST_AUTO_TEST_SUITE_END() // match_

} // namespace testing
