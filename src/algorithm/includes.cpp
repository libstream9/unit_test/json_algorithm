#include <stream9/json/algorithm/includes.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(includes_)

    BOOST_AUTO_TEST_SUITE(value_)

        BOOST_AUTO_TEST_CASE(null_1_)
        {
            json::value v1;
            json::value v2;

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(null_2_)
        {
            json::value v1;
            json::value v2 = 1;

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(bool_1_)
        {
            json::value v1 = true;
            json::value v2 = true;

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(bool_2_)
        {
            json::value v1 = true;
            json::value v2 = false;

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(int64_1_)
        {
            json::value v1 = (int64_t)1;
            json::value v2 = (int64_t)1;

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(int64_2_)
        {
            json::value v1 = (int64_t)1;
            json::value v2 = (int64_t)2;

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(uint64_1_)
        {
            json::value v1 = (uint64_t)1;
            json::value v2 = (uint64_t)1;

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(uint64_2_)
        {
            json::value v1 = (uint64_t)1;
            json::value v2 = (uint64_t)2;

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(double_1_)
        {
            json::value v1 = (double)1.0;
            json::value v2 = (double)1.0;

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(double_2_)
        {
            json::value v1 = (double)1.0;
            json::value v2 = (double)2.0;

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(string_1_)
        {
            json::value v1 = "foo";
            json::value v2 = "foo";

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(string_2_)
        {
            json::value v1 = "foo";
            json::value v2 = "fo"; // substring doesn't count

            BOOST_TEST(!json::includes(v1, v2));
        }

    BOOST_AUTO_TEST_SUITE_END() // value_

    BOOST_AUTO_TEST_SUITE(array_)

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            json::array v1 {};
            json::array v2 {};

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            json::array v1 { 1 };
            json::array v2 {};

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(empty_3_)
        {
            json::array v1 {};
            json::array v2 { 1 };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(one_element_1_)
        {
            json::array v1 { 1 };
            json::array v2 { 1 };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(one_element_2_)
        {
            json::array v1 { 1 };
            json::array v2 { 2 };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_element_1_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 1 };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_element_2_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 3 };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_element_3_)
        {
            json::array v1 { 1,};
            json::array v2 { 1, 2 };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_element_4_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 1, 2 };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_element_5_)
        {
            json::array v1 { 2, 1 };
            json::array v2 { 1, 2 };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_element_6_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 1, 3 };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_element_7_)
        {
            json::array v1 { 1, 2 };
            json::array v2 { 4, 3 };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(three_element_1_)
        {
            json::array v1 { 1, 2, 3 };
            json::array v2 { 1, 1 };

            BOOST_TEST(!json::includes(v1, v2));
        }

    BOOST_AUTO_TEST_SUITE_END() // array_

    BOOST_AUTO_TEST_SUITE(object_)

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            json::object v1;
            json::object v2;

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            json::object v1;
            json::object v2 { { "x", 1 } };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(empty_3_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2;

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(one_element_1_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "x", 1 } };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(one_element_2_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "x", 2 } };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(one_element_3_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "y", 1 } };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_elements_1_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 1 } };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_elements_2_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 2 } };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_elements_3_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "z", 1 } };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_elements_4_)
        {
            json::object v1 { { "x", 1 } };
            json::object v2 { { "x", 1 }, { "y", 1 } };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_elements_5_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 1 }, { "y", 1 } };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_elements_6_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 2 }, { "y", 1 } };

            BOOST_TEST(!json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(two_elements_8_)
        {
            json::object v1 { { "x", 1 }, { "y", 1 } };
            json::object v2 { { "x", 1 }, { "z", 1 } };

            BOOST_TEST(!json::includes(v1, v2));
        }

    BOOST_AUTO_TEST_SUITE_END() // object_

    BOOST_AUTO_TEST_SUITE(recursive_)

        BOOST_AUTO_TEST_CASE(array_array_1_)
        {
            json::array v1 {
                1,
                { 1, 2 },
            };

            json::array v2 {
                { 2 },
            };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(array_object_1_)
        {
            json::array v1 {
                1,
                {
                    { "x", 1 },
                    { "y", 2 },
                },
            };

            json::array v2 {
                {
                    { "y", 2 },
                },
            };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(object_object_1_)
        {
            json::object v1 {
                { "a",
                    {
                        { "x", 1 },
                        { "y", 2 },
                    },
                },
                { "b", 1 },
            };

            json::object v2 {
                { "a",
                    {
                        { "y", 2 },
                    },
                },
            };

            BOOST_TEST(json::includes(v1, v2));
        }

        BOOST_AUTO_TEST_CASE(object_array_1_)
        {
            json::object v1 {
                { "a", { 1, 2 } },
                { "b", 1 },
            };

            json::object v2 {
                { "a", { 2 } },
            };

            BOOST_TEST(json::includes(v1, v2));
        }

    BOOST_AUTO_TEST_SUITE_END() // recursive_

BOOST_AUTO_TEST_SUITE_END() // includes_

} // namespace testing
