#include <stream9/json/patch.hpp>

#include "namespace.hpp"

#include <stream9/json/parse.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace json::literals;

BOOST_AUTO_TEST_SUITE(patch_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(default_)
    {
        json::patch p;

        BOOST_TEST(p.empty());
    }

    BOOST_AUTO_TEST_CASE(from_json_document_)
    {
        auto const json = R"([
            { "op": "add", "path": "/", "value": 1 }
        ])";

        json::patch p { json };

        BOOST_TEST(p.size() == 1);
        BOOST_TEST(p[0] == R"({"op":"add","path":"/","value":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(from_json_value_)
    {
        auto const json = R"([
            { "op": "add", "path": "/", "value": 1 }
        ])";

        auto const v = json::parse(json);

        json::patch p { v };

        BOOST_TEST(p.size() == 1);
        BOOST_TEST(p[0] == R"({"op":"add","path":"/","value":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(by_diffing_values_)
    {
        auto v1 = R"({ "foo": 1 })"_js;
        auto v2 = R"({ "foo": 2 })"_js;

        json::patch p { v1, v2 };

        auto expected = R"([
            { "op": "test", "path": "/foo", "value": 1 },
            { "op": "replace", "path": "/foo", "value": 2 }
        ])"_js;

        BOOST_TEST(p == expected);
    }

    BOOST_AUTO_TEST_CASE(json_syntax_error_)
    {
        auto const json = R"([
            { "op": "add, "path": "/", "value": 1 }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { json },
            stream9::error,
            [](auto&&) { return true; }
        );
    }

    BOOST_AUTO_TEST_CASE(patch_is_not_array_)
    {
        auto const json = R"(
            { "op": "add", "path": "/", "value": 1 }
        )";

        BOOST_CHECK_EXCEPTION(
            json::patch p { json },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "patch document has to be an array",
                    "value": { "op": "add", "path": "/", "value": 1 }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(non_object_member_in_patch_)
    {
        auto const json = R"([1])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { json },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "operation is not an object",
                    "value": 1
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(no_op_member_in_operation_)
    {
        auto const json = R"([
            { "path": "/", "value": 1 }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { json },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "operation does not contain op member",
                    "value": { "path": "/", "value": 1 }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(unknown_op_type_will_be_ignored_)
    {
        auto const json = R"([
            { "op": "xxx", "path": "/", "value": 1 }
        ])";

        json::patch p { json };

        BOOST_TEST(p.size() == 1);
        BOOST_TEST(p[0] == R"({ "op": "xxx", "path": "/", "value": 1 })"_js);
    }

    BOOST_AUTO_TEST_CASE(no_path_member_)
    {
        auto const doc = R"([
            { "op": "xxx" }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "operation does not contain path member",
                    "value": { "op": "xxx" }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_path_member_1_)
    {
        auto const doc = R"([
            { "op": "xxx", "path": 1 }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "path member has to be a string",
                    "value": { "op": "xxx", "path": 1 }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_path_member_2_)
    {
        auto const doc = R"([
            { "op": "xxx", "path": "xxx" }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "path member is not proper json pointer",
                    "value": { "op": "xxx", "path": "xxx" },
                    "nested_error": {
                        "type": "stream9::json::pointer::error",
                        "code": "pointer has to start with '/'",
                        "pointer": "xxx",
                        "pos": 0
                    }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(add_operation_vaild_)
    {
        auto const doc = R"([
            { "op": "add", "path": "", "value": 1 }
        ])";

        json::patch p { doc };

        BOOST_TEST(p == json::parse(doc));
    }

    BOOST_AUTO_TEST_CASE(add_operation_no_value_)
    {
        auto const doc = R"([
            { "op": "add", "path": "" }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "operation does not contain value member",
                    "value": { "op": "add", "path": "" }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(remove_operation_vaild_)
    {
        auto const doc = R"([
            { "op": "remove", "path": "/x" }
        ])";

        json::patch p { doc };

        BOOST_TEST(p == json::parse(doc));
    }

    BOOST_AUTO_TEST_CASE(remove_operation_error_1_)
    {
        auto const doc = R"([
            { "op": "remove", "path": "" }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "empty path on remove operation",
                    "value": { "op": "remove", "path": "" }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(replace_operation_vaild_)
    {
        auto const doc = R"([
            { "op": "replace", "path": "", "value": null }
        ])";

        json::patch p { doc };

        BOOST_TEST(p == json::parse(doc));
    }

    BOOST_AUTO_TEST_CASE(replace_operation_no_value_)
    {
        auto const doc = R"([
            { "op": "replace", "path": "" }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "operation does not contain value member",
                    "value": { "op": "replace", "path": "" }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(move_operation_vaild_)
    {
        auto const doc = R"([
            { "op": "move", "from": "", "path": "" }
        ])";

        json::patch p { doc };

        BOOST_TEST(p == json::parse(doc));
    }

    BOOST_AUTO_TEST_CASE(move_operation_no_from_member_)
    {
        auto const doc = R"([
            { "op": "move", "src": "", "path": "" }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "operation does not contain from member",
                    "value": { "op": "move", "src": "", "path": "" }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(move_operation_from_is_proper_prefix_of_path_)
    {
        auto const doc = R"([
            { "op": "move", "from": "", "path": "/x" }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "from location can not be proper prefix of path location",
                    "value": { "op": "move", "from": "", "path": "/x" }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(copy_operation_vaild_)
    {
        auto const doc = R"([
            { "op": "copy", "from": "/", "path": "/x" }
        ])";

        json::patch p { doc };

        BOOST_TEST(p == json::parse(doc));
    }

    BOOST_AUTO_TEST_CASE(copy_operation_no_from_member_)
    {
        auto const doc = R"([
            { "op": "copy", "path": "" }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "operation does not contain from member",
                    "value": { "op": "copy", "path": "" }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(test_operation_vaild_)
    {
        auto const doc = R"([
            { "op": "test", "path": "", "value": null }
        ])";

        json::patch p { doc };

        BOOST_TEST(p == json::parse(doc));
    }

    BOOST_AUTO_TEST_CASE(test_operation_no_value_)
    {
        auto const doc = R"([
            { "op": "test", "path": "", "Value": null }
        ])";

        BOOST_CHECK_EXCEPTION(
            json::patch p { doc },
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "operation does not contain value member",
                    "value": { "op": "test", "path": "", "Value": null }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(modifiers_)

    BOOST_AUTO_TEST_CASE(add_)
    {
        json::patch p;
        p.add("/foo"_jp, 1);

        BOOST_TEST(p == R"([{"op":"add","path":"/foo","value":1}])"_js);
    }

    BOOST_AUTO_TEST_CASE(remove_)
    {
        json::patch p;
        p.remove("/bar"_jp);

        BOOST_TEST(p == R"([{"op":"remove","path":"/bar"}])"_js);
    }

    BOOST_AUTO_TEST_CASE(remove_error_)
    {
        json::patch p;

        BOOST_CHECK_EXCEPTION(
            p.remove(""_jp),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "empty path on remove operation",
                    "value": { "op": "remove", "path": "" }
                })"_js;
                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(replace_)
    {
        json::patch p;
        p.replace("/foo"_jp, 1);

        BOOST_TEST(p == R"([{"op":"replace","path":"/foo","value":1}])"_js);
    }

    BOOST_AUTO_TEST_CASE(move_success_1_)
    {
        json::patch p;
        p.move("/foo"_jp, "/bar"_jp);

        BOOST_TEST(p == R"([{"op":"move","from":"/foo","path":"/bar"}])"_js);
    }

    BOOST_AUTO_TEST_CASE(move_success_2_)
    {
        json::patch p;
        p.move("/foo"_jp, "/foo"_jp);

        BOOST_TEST(p == R"([{"op":"move","from":"/foo","path":"/foo"}])"_js);
    }

    BOOST_AUTO_TEST_CASE(move_error_)
    {
        json::patch p;

        BOOST_CHECK_EXCEPTION(
            p.move("/foo"_jp, "/foo/bar"_jp),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "from location can not be proper prefix of path location",
                    "value": { "from": "/foo", "path": "/foo/bar" }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(copy_)
    {
        json::patch p;
        p.copy("/foo"_jp, "/bar"_jp);

        BOOST_TEST(p == R"([{"op":"copy","from":"/foo","path":"/bar"}])"_js);
    }

    BOOST_AUTO_TEST_CASE(test_)
    {
        json::patch p;
        p.test("/foo"_jp, 1);

        BOOST_TEST(p == R"([{"op":"test","path":"/foo","value":1}])"_js);
    }

BOOST_AUTO_TEST_SUITE_END() // modifiers_

BOOST_AUTO_TEST_SUITE(add_)

    BOOST_AUTO_TEST_CASE(null_to_empty_value_with_empty_pointer_)
    {
        json::patch p;
        p.add(""_jp, json::value());

        json::value v;
        auto rv = json::apply_patch(v, p);

        BOOST_TEST(rv.is_null());
    }

    BOOST_AUTO_TEST_CASE(scalar_to_empty_value_with_empty_pointer_)
    {
        json::patch p;
        p.add(""_jp, 1);

        json::value v;
        auto rv = json::apply_patch(v, p);

        BOOST_TEST(rv == 1);
    }

    BOOST_AUTO_TEST_CASE(scalar_to_scalar_with_empty_pointer_)
    {
        json::patch p;
        p.add(""_jp, 1);

        json::value v = 2;
        auto rv = json::apply_patch(v, p);

        BOOST_TEST(rv == 1);
    }

    BOOST_AUTO_TEST_CASE(null_to_object_with_empty_key_)
    {
        json::patch p;
        p.add("/"_jp, json::value());

        auto v = R"({})"_js;
        auto rv = json::apply_patch(v, p);

        BOOST_TEST(rv == R"({"":null})"_js);
    }

    BOOST_AUTO_TEST_CASE(null_to_array_with_empty_key_)
    {
        json::patch p;
        p.add("/"_jp, json::value());

        auto v = R"([])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "add", "path": "/", "value": null },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(insert_scalar_to_object_)
    {
        json::patch p;
        p.add("/1"_jp, 1);

        auto v = R"({})"_js;
        auto rv = json::apply_patch(v, p);

        BOOST_TEST(rv == R"({"1":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(replace_value_on_object_with_scalar_)
    {
        json::patch p;
        p.add("/-"_jp, 1);

        auto v = R"({"-":null})"_js;
        auto rv = json::apply_patch(v, p);

        BOOST_TEST(rv == R"({"-":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_1_)
    {
        json::patch p;
        p.add("/0"_jp, 9);

        auto v = R"([1,2,3])"_js;
        auto rv = json::apply_patch(v, p);

        BOOST_TEST(rv == R"([9,1,2,3])"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_2_)
    {
        json::patch p;
        p.add("/3"_jp, 9);

        auto v = R"([1,2,3])"_js;
        auto rv = json::apply_patch(v, p);

        BOOST_TEST(rv == R"([1,2,3,9])"_js);
    }

    BOOST_AUTO_TEST_CASE(insert_value_on_array_3_)
    {
        json::patch p;
        p.add("/-"_jp, 9);

        auto v = R"([1,2,3])"_js;
        auto rv = json::apply_patch(v, p);

        BOOST_TEST(rv == R"([1,2,3,9])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_index_out_of_bound_)
    {
        json::patch p;
        p.add("/4"_jp, 9);

        auto v = R"([1,2,3])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "add", "path": "/4", "value": 9 },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "array index out of bound",
                    "pointer": "/4",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(invalid_parent_)
    {
        json::patch p;
        p.add("/foo/0"_jp, 9);

        auto v1 = R"({"foo":1})"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::apply_patch(v1, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "pointer error",
                    "value": { "op": "add", "path": "/foo/0", "value": 9 },
                    "nested_error": {
                      "type": "stream9::json::pointer::error",
                      "code": "array index on scalar",
                      "pointer": "/foo/0",
                      "pos": 5
                    }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(invalid_path_)
    {
        json::patch p;
        p.add("/foo/0/-"_jp, 9);

        auto v1 = R"({"foo":{}})"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::apply_patch(v1, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "add", "path": "/foo/0/-", "value": 9 },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "object key does not exist",
                    "pointer": "/foo/0/-",
                    "pos": 5
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(with_unknown_operation_)
    {
        auto doc = R"([
            { "op": "add", "path": "", "value": 1 },
            { "op": "xxx", "path": "" }
        ])";

        json::patch p { doc };

        json::value v1;

        auto const rv = json::apply_patch(v1, p);

        BOOST_TEST(rv == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // add_

BOOST_AUTO_TEST_SUITE(remove_)

    BOOST_AUTO_TEST_CASE(object_1_)
    {
        json::patch p;
        p.remove("/foo"_jp);

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"({})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_2_)
    {
        json::patch p;
        p.remove("/foo/bar"_jp);

        auto v = R"({"foo":{"bar":1}})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"({"foo":{}})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_error_1_)
    {
        json::patch p;
        p.remove("/bar"_jp);

        auto v = R"({"foo":1})"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "remove", "path": "/bar" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "object key does not exist",
                    "pointer": "/bar",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_1_)
    {
        json::patch p;
        p.remove("/0"_jp);

        auto v = R"([0])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"([])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_2_)
    {
        json::patch p;
        p.remove("/0/1"_jp);

        auto v = R"([[0,1],2])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"([[0],2])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_error_1_)
    {
        json::patch p;
        p.remove("/2"_jp);

        auto v = R"([1,2])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "remove", "path": "/2" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "array index out of bound",
                    "pointer": "/2",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_error_2_)
    {
        json::patch p;
        p.remove("/x"_jp);

        auto v = R"([])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "remove", "path": "/x" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/x",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // remove_

BOOST_AUTO_TEST_SUITE(replace_)

    BOOST_AUTO_TEST_CASE(empty_pointer_empty_value_)
    {
        json::patch p;
        p.replace(""_jp, json::value());

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv.is_null());
    }

    BOOST_AUTO_TEST_CASE(empty_pointer_)
    {
        json::patch p;
        p.replace(""_jp, 1);

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == 1);
    }

    BOOST_AUTO_TEST_CASE(object_1_)
    {
        json::patch p;
        p.replace("/x"_jp, 1);

        auto v = R"({"x":null})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"({"x":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_2_)
    {
        json::patch p;
        p.replace("/x/x"_jp, 1);

        auto v = R"({"x":{"x":null}})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"({"x":{"x":1}})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_error_1_)
    {
        json::patch p;
        p.replace("/x"_jp, 1);

        auto v = R"({})"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "replace", "path": "/x", "value": 1 },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "object key does not exist",
                    "pointer": "/x",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_1_)
    {
        json::patch p;
        p.replace("/0"_jp, 1);

        auto v = R"([0])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"([1])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_2_)
    {
        json::patch p;
        p.replace("/0/1"_jp, 1);

        auto v = R"([[2,0],1])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"([[2,1],1])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_error_1_)
    {
        json::patch p;
        p.replace("/0"_jp, 1);

        auto v = R"([])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "replace", "path": "/0", "value": 1 },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "array index out of bound",
                    "pointer": "/0",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_error_2_)
    {
        json::patch p;
        p.replace("/x"_jp, 1);

        auto v = R"([])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "replace", "path": "/x", "value": 1 },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/x",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // replace_

BOOST_AUTO_TEST_SUITE(move_)

    BOOST_AUTO_TEST_CASE(empty_pointer_to_empty_pointer_)
    {
        json::patch p;
        p.move(""_jp, ""_jp);

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == v);
    }

    BOOST_AUTO_TEST_CASE(object_insert_)
    {
        json::patch p;
        p.move("/foo"_jp, "/bar"_jp);

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"({"bar":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_replace_)
    {
        json::patch p;
        p.move("/foo"_jp, "/bar"_jp);

        auto v = R"({"foo":1,"bar":null})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"({"bar":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_move_to_parent_)
    {
        json::patch p;
        p.move("/foo"_jp, ""_jp);

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == 1);
    }

    BOOST_AUTO_TEST_CASE(object_error_1_)
    {
        json::patch p;
        p.move("/foo"_jp, "/bar"_jp);

        auto v = R"({"bar":1})"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "move", "from": "/foo", "path": "/bar" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "object key does not exist",
                    "pointer": "/foo",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(object_error_2_)
    {
        json::patch p;
        p.move("/foo"_jp, "/bar/baz"_jp);

        auto v = R"({"foo":1})"_js;

        BOOST_CHECK_EXCEPTION(
            (void)json::apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "move", "from": "/foo", "path": "/bar/baz" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "object key does not exist",
                    "pointer": "/bar/baz",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_1_)
    {
        json::patch p;
        p.move("/1"_jp, "/0"_jp);

        auto v = R"([0,1])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"([1,0])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_move_to_parent_)
    {
        json::patch p;
        p.move("/1"_jp, ""_jp);

        auto v = R"([0,1])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == 1);
    }

    BOOST_AUTO_TEST_CASE(array_error_1_)
    {
        json::patch p;
        p.move("/1"_jp, "/2"_jp);

        auto v = R"([0,1])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "move", "from": "/1", "path": "/2" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "array index out of bound",
                    "pointer": "/2",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_error_2_)
    {
        json::patch p;
        p.move("/-"_jp, "/0"_jp);

        auto v = R"([0,1])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "move", "from": "/-", "path": "/0" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "array index out of bound",
                    "pointer": "/-",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_error_3_)
    {
        json::patch p;
        p.move("/x"_jp, "/0"_jp);

        auto v = R"([0,1])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "move", "from": "/x", "path": "/0" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/x",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_error_4_)
    {
        json::patch p;
        p.move("/0"_jp, "/x"_jp);

        auto v = R"([0,1])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "move", "from": "/0", "path": "/x" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/x",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // move_

BOOST_AUTO_TEST_SUITE(copy_)

    BOOST_AUTO_TEST_CASE(empty_pointer_to_empty_pointer_)
    {
        json::patch p;
        p.copy(""_jp, ""_jp);

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == v);
    }

    BOOST_AUTO_TEST_CASE(empty_pointer_to_object_)
    {
        json::patch p;
        p.copy(""_jp, "/bar"_jp);

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"({"foo":1,"bar":{"foo":1}})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_insert_)
    {
        json::patch p;
        p.copy("/foo"_jp, "/bar"_jp);

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"({"foo":1,"bar":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_replace_)
    {
        json::patch p;
        p.copy("/foo"_jp, "/bar"_jp);

        auto v = R"({"foo":1,"bar":null})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"({"foo":1,"bar":1})"_js);
    }

    BOOST_AUTO_TEST_CASE(object_error_1_)
    {
        json::patch p;
        p.copy("/foo"_jp, "/bar"_jp);

        auto v = R"({"bar":1})"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "copy", "from": "/foo", "path": "/bar" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "object key does not exist",
                    "pointer": "/foo",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(empty_pointer_to_array_)
    {
        json::patch p;
        p.copy(""_jp, "/1"_jp);

        auto v = R"([0,1])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"([0,[0,1],1])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_1_)
    {
        json::patch p;
        p.copy("/0"_jp, "/-"_jp);

        auto v = R"([0,1])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"([0,1,0])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_2_)
    {
        json::patch p;
        p.copy("/1"_jp, "/0"_jp);

        auto v = R"([0,1])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == R"([1,0,1])"_js);
    }

    BOOST_AUTO_TEST_CASE(array_error_1_)
    {
        json::patch p;
        p.copy("/-"_jp, "/-"_jp);

        auto v = R"([0,1])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "copy", "from": "/-", "path": "/-" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "array index out of bound",
                    "pointer": "/-",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_error_2_)
    {
        json::patch p;
        p.copy("/0"_jp, "/3"_jp);

        auto v = R"([0,1])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "copy", "from": "/0", "path": "/3" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "array index out of bound",
                    "pointer": "/3",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_error_3_)
    {
        json::patch p;
        p.copy("/x"_jp, "/-"_jp);

        auto v = R"([0,1])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "copy", "from": "/x", "path": "/-" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/x",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_error_4_)
    {
        json::patch p;
        p.copy("/0"_jp, "/x"_jp);

        auto v = R"([0,1])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "copy", "from": "/0", "path": "/x" },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/x",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // copy_

BOOST_AUTO_TEST_SUITE(test_)

    BOOST_AUTO_TEST_CASE(empty_pointer_)
    {
        json::patch p;
        p.test(""_jp, {});

        auto v = R"(null)"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == v);
    }

    BOOST_AUTO_TEST_CASE(object_1_)
    {
        json::patch p;
        p.test("/foo"_jp, 1);

        auto v = R"({"foo":1})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == v);
    }

    BOOST_AUTO_TEST_CASE(object_2_)
    {
        json::patch p;
        p.test("/foo/bar"_jp, 1);

        auto v = R"({"foo":{"bar":1}})"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == v);
    }

    BOOST_AUTO_TEST_CASE(object_error_1_)
    {
        json::patch p;
        p.test("/foo"_jp, 1);

        auto v = R"({"foo":2})"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "tested value does not match",
                    "value": { "op": "test", "path": "/foo", "value": 1 }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(object_error_2_)
    {
        json::patch p;
        p.test("/foo"_jp, 1);

        auto v = R"({"bar":1})"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "test", "path": "/foo", "value": 1 },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "object key does not exist",
                    "pointer": "/foo",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_1_)
    {
        json::patch p;
        p.test("/0"_jp, 1);

        auto v = R"([1])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == v);
    }

    BOOST_AUTO_TEST_CASE(array_2_)
    {
        json::patch p;
        p.test("/0/0"_jp, 1);

        auto v = R"([[1]])"_js;

        auto rv = apply_patch(v, p);

        BOOST_TEST(rv == v);
    }

    BOOST_AUTO_TEST_CASE(array_error_1_)
    {
        json::patch p;
        p.test("/0"_jp, 1);

        auto v = R"([2])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                    "type": "stream9::json::patch::error",
                    "code": "tested value does not match",
                    "value": { "op": "test", "path": "/0", "value": 1 }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            }
        );
    }

    BOOST_AUTO_TEST_CASE(array_error_2_)
    {
        json::patch p;
        p.test("/-"_jp, 1);

        auto v = R"([0])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "test", "path": "/-", "value": 1 },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "array index out of bound",
                    "pointer": "/-",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(array_error_3_)
    {
        json::patch p;
        p.test("/x"_jp, 1);

        auto v = R"([0])"_js;

        BOOST_CHECK_EXCEPTION(
            (void)apply_patch(v, p),
            json::patch::error,
            [](auto&& e) {
                auto expected = R"({
                  "type": "stream9::json::patch::error",
                  "code": "pointer error",
                  "value": { "op": "test", "path": "/x", "value": 1 },
                  "nested_error": {
                    "type": "stream9::json::pointer::error",
                    "code": "invalid array index",
                    "pointer": "/x",
                    "pos": 1
                  }
                })"_js;

                BOOST_TEST(json::value_from(e) == expected);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // test_

BOOST_AUTO_TEST_SUITE_END() // patch_

} // namespace testing
