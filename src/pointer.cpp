#include <stream9/json/pointer.hpp>

#include "namespace.hpp"

#include <concepts>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace json = stream9::json;

using boost::test_tools::per_element;
using E = json::pointer::errc;

BOOST_AUTO_TEST_SUITE(pointer_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        static_assert(std::regular<json::pointer>);
        static_assert(std::ranges::contiguous_range<json::pointer>);
    }

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(default_)
        {
            json::pointer p;

            BOOST_TEST(p.empty());
            BOOST_TEST(p == "");
        }

        BOOST_AUTO_TEST_CASE(empty_string_)
        {
            json::pointer p { "" };

            BOOST_TEST(p.empty());
            BOOST_TEST(p == "");
        }

        BOOST_AUTO_TEST_CASE(one_element_)
        {
            json::pointer p { "/foo" };

            BOOST_TEST(p.size() == 1);
            BOOST_TEST(p == "/foo");
        }

        BOOST_AUTO_TEST_CASE(many_elements_)
        {
            json::pointer p { "/foo/bar/1" };

            BOOST_TEST(p.size() == 3);
            BOOST_TEST(p == "/foo/bar/1");
        }

        BOOST_AUTO_TEST_CASE(error_1_)
        {
            BOOST_CHECK_EXCEPTION(
                json::pointer p { "foo" },
                json::pointer::error,
                [](auto&& e) {
                    BOOST_TEST(json::value_from(e) == R"({
                        "type": "stream9::json::pointer::error",
                        "code": "pointer has to start with '/'",
                        "pointer": "foo",
                        "pos": 0
                    })"_js );
                    return true;
                } );
        }

        BOOST_AUTO_TEST_CASE(error_2_)
        {
            BOOST_CHECK_EXCEPTION(
                json::pointer p { "/foo~" },
                json::pointer::error,
                [](auto&& e) {
                    BOOST_TEST(json::value_from(e) == R"({
                        "type": "stream9::json::pointer::error",
                        "code": "incomplete escape sequence",
                        "pointer": "/foo~",
                        "pos": 5
                    })"_js );
                    return true;
                } );
        }

        BOOST_AUTO_TEST_CASE(error_3_)
        {
            BOOST_CHECK_EXCEPTION(
                json::pointer p { "/foo/~3bar" },
                json::pointer::error,
                [](auto&& e) {
                    auto expected = R"({
                        "type": "stream9::json::pointer::error",
                        "code": "unknown escape sequence",
                        "pointer": "/foo/~3bar",
                        "pos": 6
                    })"_js;
                    BOOST_TEST(json::value_from(e) == expected);
                    return true;
                } );
        }

        BOOST_AUTO_TEST_CASE(leading_zero_is_not_error_until_evaluated_against_array_)
        {
            BOOST_CHECK_NO_THROW(
                json::pointer p { "/0/1/02" }
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_SUITE(accessor_)

        BOOST_AUTO_TEST_CASE(iterator_)
        {
            json::pointer p { "/foo/bar/1" };

            auto const expected = { "foo", "bar", "1" };

            BOOST_TEST(p == expected, per_element());
        }

        BOOST_AUTO_TEST_CASE(subscript_)
        {
            json::pointer p { "/foo~1/bar/1" };

            BOOST_TEST(p[1] == "bar");
        }

        BOOST_AUTO_TEST_CASE(at_)
        {
            json::pointer p { "/foo~1/bar/1" };

            BOOST_TEST(p.at(0) == "foo/");

            BOOST_CHECK_THROW(p.at(3), std::out_of_range);
        }

    BOOST_AUTO_TEST_SUITE_END() // accessor_

    BOOST_AUTO_TEST_SUITE(push_back_)

        BOOST_AUTO_TEST_CASE(empty_key_)
        {
            json::pointer p;

            p.push_back("");

            BOOST_TEST(p.size() == 1);
            BOOST_TEST(p == "/");
        }

        BOOST_AUTO_TEST_CASE(key_)
        {
            json::pointer p { "/foo" };

            p.push_back("~bar");

            BOOST_TEST(p.size() == 2);
            BOOST_TEST(p == "/foo/~0bar");
        }

        BOOST_AUTO_TEST_CASE(index_)
        {
            json::pointer p;

            p.push_back(0);
            p.push_back(5);

            BOOST_TEST(p.size() == 2);
            BOOST_TEST(p == "/0/5");
        }

    BOOST_AUTO_TEST_SUITE_END() // push_back_

    BOOST_AUTO_TEST_CASE(pop_back_)
    {
        json::pointer p { "/foo/bar/0/baz~1" };

        BOOST_TEST(p.size() == 4);

        p.pop_back();
        BOOST_TEST(p == "/foo/bar/0");

        p.pop_back();
        BOOST_TEST(p == "/foo/bar");

        p.pop_back();
        BOOST_TEST(p == "/foo");

        p.pop_back();
        BOOST_TEST(p.empty());
    }

    BOOST_AUTO_TEST_SUITE(insert_)

        BOOST_AUTO_TEST_CASE(empty_key_)
        {
            json::pointer p { "/foo" };

            auto it = p.insert(p.begin(), "");
            BOOST_TEST(*it == "");

            BOOST_TEST(p == "//foo");
        }

        BOOST_AUTO_TEST_CASE(key_)
        {
            json::pointer p { "/foo" };

            auto it = p.insert(p.begin(), "before");
            BOOST_TEST(*it == "before");

            it = p.insert(p.end(), "after");
            BOOST_TEST(*it == "after");

            it = p.insert(it, "middle");
            BOOST_TEST(*it == "middle");

            BOOST_TEST(p == "/before/foo/middle/after");
        }

        BOOST_AUTO_TEST_CASE(index_)
        {
            json::pointer p { "/foo" };

            auto it = p.insert(p.begin(), 0);
            BOOST_TEST(*it == "0");

            it = p.insert(p.end(), 100);
            BOOST_TEST(*it == "100");

            it = p.insert(it, 50);
            BOOST_TEST(*it == "50");

            BOOST_TEST(p == "/0/foo/50/100");
        }

    BOOST_AUTO_TEST_SUITE_END() // insert_

    BOOST_AUTO_TEST_SUITE(erase_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            json::pointer p { "/foo/bar" };

            auto it = p.erase(p.begin());
            BOOST_TEST((it == p.begin()));
            BOOST_TEST(p == "/bar");
        }

    BOOST_AUTO_TEST_SUITE_END() // erase_

    BOOST_AUTO_TEST_CASE(clear_)
    {
        json::pointer p { "/0/1/2/3" };

        BOOST_TEST(p.size() == 4);

        p.clear();

        BOOST_TEST(p.empty());
    }

    BOOST_AUTO_TEST_SUITE(to_string_)

        BOOST_AUTO_TEST_CASE(function_)
        {
            json::pointer p1 { "/foo/bar" };

            BOOST_TEST(p1.to_string() == "/foo/bar");

            json::pointer p2;

            BOOST_TEST(p2.to_string().empty());
        }

        BOOST_AUTO_TEST_CASE(operator_)
        {
            json::pointer p { "/foo/bar" };

            BOOST_TEST(std::string(p) == "/foo/bar");
        }

    BOOST_AUTO_TEST_SUITE_END() // to_string_

    BOOST_AUTO_TEST_SUITE(to_value_)

        BOOST_AUTO_TEST_CASE(function_)
        {
            json::pointer p1 { "/foo/bar" };
            auto v1 = p1.to_value();

            BOOST_TEST_REQUIRE(v1.is_string());
            BOOST_TEST(v1 == "/foo/bar");

            json::pointer p2;
            auto v2 = p2.to_value();

            BOOST_TEST_REQUIRE(v2.is_string());
            BOOST_TEST(v2.get_string().empty());
        }

        BOOST_AUTO_TEST_CASE(operator_)
        {
            json::pointer p { "/foo/bar" };

            BOOST_TEST(json::value(p) == "/foo/bar");
        }

    BOOST_AUTO_TEST_SUITE_END() // to_value_

    BOOST_AUTO_TEST_SUITE(equal_)

        BOOST_AUTO_TEST_CASE(with_pointer_)
        {
            json::pointer p1 { "/foo" };
            json::pointer p2 { "/foo" };
            json::pointer p3 { "/bar" };

            BOOST_TEST(p1 == p2);
            BOOST_TEST(p1 != p3);
        }

        BOOST_AUTO_TEST_CASE(with_string_)
        {
            json::pointer p1 { "/foo" };

            BOOST_TEST(p1 == "/foo");
            BOOST_TEST(p1 != "/bar");

            BOOST_TEST("/foo" == p1);
            BOOST_TEST("/bar" != p1);
        }

    BOOST_AUTO_TEST_SUITE_END() // equal_

    BOOST_AUTO_TEST_SUITE(three_way_)

        BOOST_AUTO_TEST_CASE(with_pointer_)
        {
            json::pointer p1 { "/foo" };
            json::pointer p2 { "/foo" };
            json::pointer p3 { "/bar" };

            BOOST_CHECK((p1 <=> p2) == std::strong_ordering::equal);
            BOOST_CHECK((p1 <=> p3) == std::strong_ordering::greater);
            BOOST_CHECK((p3 <=> p1) == std::strong_ordering::less);
        }

        BOOST_AUTO_TEST_CASE(with_empty_pointer_)
        {
            json::pointer p1;
            json::pointer p2 { "/foo" };

            BOOST_CHECK((p1 <=> json::pointer()) == std::strong_ordering::equal);
            BOOST_CHECK((json::pointer() <=> p2) == std::strong_ordering::less);
            BOOST_CHECK((p2 <=> json::pointer()) == std::strong_ordering::greater);
        }

        BOOST_AUTO_TEST_CASE(with_string_)
        {
            json::pointer p1 { "/foo" };
            json::pointer p2 { "/bar" };

            BOOST_CHECK((p1 <=> "/foo") == std::strong_ordering::equal);
            BOOST_CHECK((p1 <=> "/bar") == std::strong_ordering::greater);
            BOOST_CHECK((p2 <=> "/foo") == std::strong_ordering::less);

            BOOST_CHECK(("/foo" <=> p1) == std::strong_ordering::equal);
            BOOST_CHECK(("/foo" <=> p2) == std::strong_ordering::greater);
            BOOST_CHECK(("/bar" <=> p1) == std::strong_ordering::less);
        }

        BOOST_AUTO_TEST_CASE(with_empty_string_)
        {
            json::pointer p1;
            json::pointer p2 { "/bar" };

            BOOST_CHECK((p1 <=> "") == std::strong_ordering::equal);
            BOOST_CHECK((p2 <=> "") == std::strong_ordering::greater);

            BOOST_CHECK(("" <=> p1) == std::strong_ordering::equal);
            BOOST_CHECK(("" <=> p2) == std::strong_ordering::less);
        }

    BOOST_AUTO_TEST_SUITE_END() // threeway_

    BOOST_AUTO_TEST_SUITE(slash_operator_)

        BOOST_AUTO_TEST_CASE(compound_assignment_key_)
        {
            json::pointer p1;

            auto& r = p1 /= "foo";
            BOOST_CHECK(&r == &p1);

            BOOST_TEST(p1 == "/foo");
        }

        BOOST_AUTO_TEST_CASE(compound_assignment_index_)
        {
            json::pointer p1 { "/foo" };

            auto& r = p1 /= 100;
            BOOST_CHECK(&r == &p1);

            BOOST_TEST(p1 == "/foo/100");
        }

        BOOST_AUTO_TEST_CASE(key_)
        {
            json::pointer p1 { "/foo" };

            auto r1 = p1 / "bar~";

            BOOST_TEST(p1 == "/foo");
            BOOST_TEST(r1 == "/foo/bar~0");

            auto r2 = "bar/" / p1;

            BOOST_TEST(p1 == "/foo");
            BOOST_TEST(r2 == "/bar~1/foo");
        }

        BOOST_AUTO_TEST_CASE(index_)
        {
            json::pointer p1;

            auto r1 = p1 / 0;

            BOOST_TEST(p1.empty());
            BOOST_TEST(r1 == "/0");

            auto r2 = 100 / p1;

            BOOST_TEST(p1.empty());
            BOOST_TEST(r2 == "/100");
        }

    BOOST_AUTO_TEST_SUITE_END() // slash_operator_

    BOOST_AUTO_TEST_CASE(literal_)
    {
        using namespace json::literals;

        auto p1 = "/foo/bar/100"_jp;

        static_assert(std::same_as<decltype(p1), json::pointer>);

        BOOST_TEST(p1 == "/foo/bar/100");
    }

BOOST_AUTO_TEST_SUITE_END() // pointer_

} // namespace testing
